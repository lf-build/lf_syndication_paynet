﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Paynet.Api
{
    /// <summary>
    /// Settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Service Name
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "paynet";
    }
}