﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Proxy;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;

namespace LendFoundry.Paynet.Api {
    internal class Startup {
        #region Public Methods

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
             app.UseHealthCheck();
		app.UseCors(env);
            app.UseSwagger ();

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "Paynet Service");
            });
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            app.UseMvc ();
        }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices (IServiceCollection services) {
            services.AddSwaggerGen (c => {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                        Title = "Paynet"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme () {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "LendFoundry.Paynet.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();
            // services
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddTenantTime ();

            // aspnet mvc related
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();

            // interface implements
            services.AddConfigurationService<PaynetConfiguration> (Settings.ServiceName);
            services.AddDependencyServiceUriResolver<PaynetConfiguration> (Settings.ServiceName);
            services.AddEventHub (Settings.ServiceName);
            services.AddTenantService ();
            // Configuration factory
            services.AddTransient<IPaynetConfiguration> (p => {
              //  ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var configuration = p.GetService<IConfigurationService<PaynetConfiguration>> ().Get ();
                // configuration.ProxyUrl = $"http://{Settings.TlsProxy.Host}:{Settings.TlsProxy.Port}";
                return configuration;
            });
            services.AddLookupService ();
            services.AddTransient<IPaynetProxy, PaynetProxy> ();
            services.AddTransient<IPaynetService, PaynetService> ();
        }

        #endregion Public Methods
    }
}
