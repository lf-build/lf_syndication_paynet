﻿using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Request;
using LendFoundry.Syndication.Paynet.Response;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Net;

namespace LendFoundry.Paynet.Api.Controller
{
    /// <summary>
    /// Expose endpoint to access Paynet service methods
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        #region Public Constructors

        /// <summary>
        /// Parameterize Constructor
        /// </summary>
        /// <param name="service">Instance of Paynet</param>
        public ApiController(IPaynetService service)
        {
            Service = service;
        }

        #endregion Public Constructors

        #region Private Properties

        /// <summary>
        /// IPaynetService property
        /// </summary>
        private IPaynetService Service { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// End point call the method to Search organization details
        /// </summary>
        /// <param name="entityType">type of Entity</param>
        /// <param name="entityId">entity Id</param>
        /// <param name="request">contain attributes to perform request</param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/paynet/company/search")]
        [ProducesResponseType(typeof(ISearchCompanyResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> SearchCompany(string entityType, string entityId, [FromBody]SearchCompanyRequest request)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    return Ok(await Task.Run(() => Service.SearchCompany(entityType, entityId, request)));
                });
            }
            catch (PaynetException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        /// <summary>
        /// GetCompanyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("/{entitytype}/{entityid}/paynet/company/report")]
        [ProducesResponseType(typeof(IGetReportResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetCompanyReport(string entityType, string entityId, [FromBody]GetReportRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                return Ok(await Task.Run(() => Service.GetCompanyReport(entityType, entityId, request)));
            });
        }

        #endregion Public Methods
    }
}