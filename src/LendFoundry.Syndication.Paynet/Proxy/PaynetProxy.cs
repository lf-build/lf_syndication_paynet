﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Xml;
using LendFoundry.Syndication.Paynet.Proxy;
using LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce;
using Microsoft.AspNetCore.Http;

namespace LendFoundry.Syndication.Paynet.Proxy {
    public class PaynetProxy : IPaynetProxy {
        #region Public Constructors

        public PaynetProxy (IPaynetConfiguration configuration) {
            // ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            if (configuration == null)
                throw new ArgumentNullException (nameof (configuration));
            if (string.IsNullOrWhiteSpace (configuration.UserName))
                throw new ArgumentNullException (nameof (configuration.UserName));
            if (string.IsNullOrWhiteSpace (configuration.Password))
                throw new ArgumentNullException (nameof (configuration.Password));
            if (string.IsNullOrWhiteSpace (configuration.ProductOption))
                throw new ArgumentNullException (nameof (configuration.ProductOption));
            if (string.IsNullOrWhiteSpace (configuration.ProductType))
                throw new ArgumentNullException (nameof (configuration.ProductType));
            if (string.IsNullOrWhiteSpace (configuration.ReportFormat))
                throw new ArgumentNullException (nameof (configuration.ReportFormat));
            if (string.IsNullOrWhiteSpace (configuration.Score))
                throw new ArgumentNullException (nameof (configuration.Score));
            if (string.IsNullOrWhiteSpace (configuration.Version))
                throw new ArgumentNullException (nameof (configuration.Version));

            Configuration = configuration;
        }

        #endregion Public Constructors

        #region Private Properties

        private IPaynetConfiguration Configuration { get; }

        #endregion Private Properties

        #region Public Methods

        public LendFoundry.Syndication.Proxy.PayNetDirect.CompanySearchResponse SearchCompany (Proxy.SearchCompanyRequest searchRequest) {

            if (searchRequest == null)
                throw new ArgumentNullException (nameof (searchRequest));
            Syndication.Proxy.PayNetDirect.PayNetDirectSoapClient PayNetDirectSoap = new Syndication.Proxy.PayNetDirect.PayNetDirectSoapClient (Syndication.Proxy.PayNetDirect.PayNetDirectSoapClient.EndpointConfiguration.PayNetDirectSoap12, Configuration.Baseurl);
            int NewMatchThreshold = Convert.ToInt32 (searchRequest.NewMatchThreshold);
            var response = PayNetDirectSoap.CompanySearchAsync (Configuration.UserName, Configuration.Password, Configuration.Version, searchRequest.CompanyName, searchRequest.Alias, searchRequest.Address, searchRequest.City, searchRequest.StateCode, searchRequest.Phone, searchRequest.TaxId, NewMatchThreshold, searchRequest.UserField).Result;
            return response;
        }
        public LendFoundry.Syndication.Proxy.PayNetDirect.GetReportResponse GetCompanyReport (Proxy.GetReportRequest reportRequest) {
            if (reportRequest == null)
                throw new ArgumentNullException (nameof (reportRequest));
            Syndication.Proxy.PayNetDirect.PayNetDirectSoapClient PayNetDirectSoap = new Syndication.Proxy.PayNetDirect.PayNetDirectSoapClient (Syndication.Proxy.PayNetDirect.PayNetDirectSoapClient.EndpointConfiguration.PayNetDirectSoap12, Configuration.Baseurl);
            LendFoundry.Syndication.Proxy.PayNetDirect.GetReportResponse response = PayNetDirectSoap.GetReportAsync (Configuration.UserName, Configuration.Password, Configuration.Version, reportRequest.PaynetId, reportRequest.ProductType, reportRequest.ReportFormat, reportRequest.Score, reportRequest.ProductOption, reportRequest.UserField).Result;
            return response;
        }

        #endregion Public Methods
    }
}