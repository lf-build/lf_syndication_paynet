﻿using LendFoundry.Syndication.Paynet.Proxy;
using LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce;

namespace LendFoundry.Syndication.Paynet.Proxy {
    public interface IPaynetProxy {
        #region Public Methods

        LendFoundry.Syndication.Proxy.PayNetDirect.CompanySearchResponse SearchCompany (Proxy.SearchCompanyRequest searchRequest);

        LendFoundry.Syndication.Proxy.PayNetDirect.GetReportResponse GetCompanyReport (Proxy.GetReportRequest reportRequest);

        #endregion Public Methods
    }
}