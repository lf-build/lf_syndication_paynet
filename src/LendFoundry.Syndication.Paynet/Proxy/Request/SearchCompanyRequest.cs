﻿using System;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Paynet.Proxy
{
    public class SearchCompanyRequest : ISearchCompanyRequest
    {
        #region Public Constructors

        public SearchCompanyRequest()
        {
        }

        public SearchCompanyRequest(Request.ISearchCompanyRequest request, IPaynetConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            CompanyName = request.CompanyName;

            if (!string.IsNullOrEmpty(request.Alias))
                Alias = request.Alias;
            else
                Alias = string.Empty;

            Address = request.Address;
            City = request.City;
            StateCode = request.StateCode;
            Phone = request.Phone;
            TaxId = request.TaxId;

            if (!string.IsNullOrEmpty(request.UserField))
                UserField = request.UserField;
            else
                UserField = configuration.UserField;

            if (!string.IsNullOrEmpty(request.NewMatchThreshold))
                NewMatchThreshold = request.NewMatchThreshold;
            else
                NewMatchThreshold = configuration.NewMatchThreshold;
        }

        #endregion Public Constructors

        public string CompanyName { get; set; }

        public string Alias { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string StateCode { get; set; }

        public string Phone { get; set; }

        public string TaxId { get; set; }

        public string NewMatchThreshold { get; set; }

        public string UserField { get; set; }
    }
}