﻿using System;

namespace LendFoundry.Syndication.Paynet.Proxy
{
    public interface ISearchCompanyRequest
    {
        string CompanyName { get; set; }

        string Alias { get; set; }

        string Address { get; set; }

        string City { get; set; }

        string StateCode { get; set; }

        string Phone { get; set; }

        string TaxId { get; set; }

        string UserField { get; set; }

        string NewMatchThreshold { get; set; }
    }
}