﻿namespace LendFoundry.Syndication.Paynet.Proxy
{
    public interface IGetReportRequest
    {
        string PaynetId { get; set; }
        string ProductType { get; set; }
        string ReportFormat { get; set; }
        string Score { get; set; }
        string ProductOption { get; set; }
        string UserField { get; set; }
    }
}