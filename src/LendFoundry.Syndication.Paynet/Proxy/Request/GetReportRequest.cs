﻿using System;

namespace LendFoundry.Syndication.Paynet.Proxy
{
    public partial class GetReportRequest : IGetReportRequest
    {
        #region Public Constructors

        public GetReportRequest()
        {
        }

        public GetReportRequest(Request.IGetReportRequest request, IPaynetConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (request == null)
                throw new ArgumentNullException(nameof(request));

            UserName = configuration.UserName;
            Password = configuration.Password;
            Version = configuration.Version;
            PaynetId = request.PaynetId;

            if (!string.IsNullOrEmpty(request.ProductType))
                ProductType = request.ProductType;
            else
                ProductType = configuration.ProductType;

            if (!string.IsNullOrEmpty(request.ReportFormat))
                ReportFormat = request.ReportFormat;
            else
                ReportFormat = configuration.ReportFormat;

            if (!string.IsNullOrEmpty(request.Score))
                Score = request.Score;
            else
                Score = configuration.Score;

            if (!string.IsNullOrEmpty(request.ProductOption))
                ProductOption = request.ProductOption;
            else
                ProductOption = configuration.ProductOption;

            if (!string.IsNullOrEmpty(request.UserField))
                UserField = request.UserField;
            else
                UserField = configuration.UserField;
        }

        #endregion Public Constructors

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Version { get; set; }

        public string PaynetId { get; set; }

        public string ProductType { get; set; }

        public string ReportFormat { get; set; }

        public string Score { get; set; }

        public string ProductOption { get; set; }

        public string UserField { get; set; }
    }
}