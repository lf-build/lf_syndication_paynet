using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LendFoundry.Syndication.Paynet.Proxy.ReportResponse
{
    [XmlRoot(ElementName = "report_option")]
    public class Report_option
    {
        [XmlAttribute(AttributeName = "provided")]
        public string Provided { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "report_options")]
    public class Report_options
    {
        [XmlElement(ElementName = "report_option")]
        public List<Report_option> Report_option { get; set; }
    }

    [XmlRoot(ElementName = "name_address")]
    public class Name_address
    {
        [XmlElement(ElementName = "primary_name")]
        public string Primary_name { get; set; }
        [XmlElement(ElementName = "other_names")]
        public other_names Other_names { get; set; }
        [XmlElement(ElementName = "address_1")]
        public string Address_1 { get; set; }

        [XmlElement(ElementName = "address_2")]
        public string Address_2 { get; set; }

        [XmlElement(ElementName = "city")]
        public string City { get; set; }

        [XmlElement(ElementName = "state_code")]
        public string State_code { get; set; }

        [XmlElement(ElementName = "postal_code")]
        public string Postal_code { get; set; }

        [XmlElement(ElementName = "country_code")]
        public string Country_code { get; set; }

        [XmlElement(ElementName = "telephone")]
        public string Telephone { get; set; }

        [XmlElement(ElementName = "fax")]
        public string Fax { get; set; }

        [XmlElement(ElementName = "tax_id")]
        public string Tax_id { get; set; }
    }
    [XmlRoot(ElementName = "other_names")]
    public class other_names {

        [XmlElement(ElementName = "name")]
        public List<string> name { get; set; }
    }

    [XmlRoot(ElementName = "inquiry")]
    public class Inquiry
    {
        [XmlAttribute(AttributeName = "month")]
        public string Month { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "inquiries")]
    public class Inquiries
    {
        [XmlElement(ElementName = "inquiry")]
        public List<Inquiry> Inquiry { get; set; }

        [XmlAttribute(AttributeName = "total")]
        public string Total { get; set; }
    }

   

    [XmlRoot(ElementName = "contract")]
    public class Contract
    {
        [XmlElement(ElementName = "name_address")]
        public Name_address Name_address { get; set; }

        [XmlElement(ElementName = "contract_type")]
        public string Contract_type { get; set; }

        [XmlElement(ElementName = "equipment_type")]
        public string Equipment_type { get; set; }

        [XmlElement(ElementName = "guarantor_code")]
        public string Guarantor_code { get; set; }

        [XmlElement(ElementName = "term")]
        public string Term { get; set; }

        [XmlElement(ElementName = "payment_amt")]
        public string Payment_amt { get; set; }

        [XmlElement(ElementName = "payment_freq")]
        public string Payment_freq { get; set; }

        [XmlElement(ElementName = "orig_receivable_amt")]
        public string Orig_receivable_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_amt")]
        public string Cur_bal_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_pct_of_orig_receivable")]
        public string Cur_bal_pct_of_orig_receivable { get; set; }

        [XmlElement(ElementName = "past_due_3160_amt")]
        public string Past_due_3160_amt { get; set; }

        [XmlElement(ElementName = "past_due_6190_amt")]
        public string Past_due_6190_amt { get; set; }

        [XmlElement(ElementName = "past_due_91_plus_amt")]
        public string Past_due_91_plus_amt { get; set; }

        [XmlElement(ElementName = "scheduled_payments")]
        public string Scheduled_payments { get; set; }

        [XmlElement(ElementName = "past_due_3160_occurrences")]
        public string Past_due_3160_occurrences { get; set; }

        [XmlElement(ElementName = "past_due_6190_occurrences")]
        public string Past_due_6190_occurrences { get; set; }

        [XmlElement(ElementName = "past_due_91_plus_occurrences")]
        public string Past_due_91_plus_occurrences { get; set; }

        [XmlElement(ElementName = "days_past_due")]
        public string Days_past_due { get; set; }

        [XmlElement(ElementName = "avg_days_past_due")]
        public string Avg_days_past_due { get; set; }

        [XmlElement(ElementName = "max_days_past_due_low")]
        public string Max_days_past_due_low { get; set; }

        [XmlElement(ElementName = "max_days_past_due_high")]
        public string Max_days_past_due_high { get; set; }

        [XmlElement(ElementName = "loss_amt")]
        public string Loss_amt { get; set; }

        [XmlElement(ElementName = "renewal_date")]
        public string Renewal_date { get; set; }

        [XmlElement(ElementName = "closed_date")]
        public string Closed_date { get; set; }

        [XmlElement(ElementName = "as_of_date")]
        public string As_of_date { get; set; }

        [XmlElement(ElementName = "start_date")]
        public string Start_date { get; set; }

        [XmlElement(ElementName = "last_payment_date")]
        public string Last_payment_date { get; set; }

        [XmlElement(ElementName = "next_payment_due_date")]
        public string Next_payment_due_date { get; set; }

        [XmlElement(ElementName = "max_past_due_date")]
        public string Max_past_due_date { get; set; }

        [XmlElement(ElementName = "loss_status")]
        public string Loss_status { get; set; }

        [XmlElement(ElementName = "contract_open_ind")]
        public string Contract_open_ind { get; set; }

        [XmlElement(ElementName = "transferred_to_other_lender")]
        public string Transferred_to_other_lender { get; set; }

        [XmlElement(ElementName = "transferred_from_other_lender")]
        public string Transferred_from_other_lender { get; set; }

        [XmlElement(ElementName = "first_bal_date")]
        public string First_bal_date { get; set; }
    }

    [XmlRoot(ElementName = "contracts")]
    public class Contracts
    {
        [XmlElement(ElementName = "contract")]
        public List<Contract> Contract { get; set; }
    }

    [XmlRoot(ElementName = "member_lender")]
    public class Member_lender
    {
        [XmlElement(ElementName = "primary_industry")]
        public string Primary_industry { get; set; }

        [XmlElement(ElementName = "as_of_date")]
        public string As_of_date { get; set; }

        [XmlElement(ElementName = "orig_receivable_amt")]
        public string Orig_receivable_amt { get; set; }

        [XmlElement(ElementName = "high_credit_amt")]
        public string High_credit_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_amt")]
        public string Cur_bal_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_amt_pct_of_high_credit")]
        public string Cur_bal_amt_pct_of_high_credit { get; set; }

        [XmlElement(ElementName = "cur_bal_amt_pct_of_orig_receivable")]
        public string Cur_bal_amt_pct_of_orig_receivable { get; set; }

        [XmlElement(ElementName = "monthly_payment_amt")]
        public string Monthly_payment_amt { get; set; }

        [XmlElement(ElementName = "annualized_scheduled_pmts")]
        public string Annualized_scheduled_pmts { get; set; }

        [XmlElement(ElementName = "loss_amt")]
        public string Loss_amt { get; set; }

        [XmlElement(ElementName = "past_due_3160_amt")]
        public string Past_due_3160_amt { get; set; }

        [XmlElement(ElementName = "past_due_6190_amt")]
        public string Past_due_6190_amt { get; set; }

        [XmlElement(ElementName = "past_due_91_plus_amt")]
        public string Past_due_91_plus_amt { get; set; }

        [XmlElement(ElementName = "scheduled_payments")]
        public string Scheduled_payments { get; set; }

        [XmlElement(ElementName = "past_due_3160_occurrences")]
        public string Past_due_3160_occurrences { get; set; }

        [XmlElement(ElementName = "past_due_6190_occurrences")]
        public string Past_due_6190_occurrences { get; set; }

        [XmlElement(ElementName = "past_due_91_plus_occurrences")]
        public string Past_due_91_plus_occurrences { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_3160_date")]
        public string Most_recent_past_due_3160_date { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_6190_date")]
        public string Most_recent_past_due_6190_date { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_91_plus_date")]
        public string Most_recent_past_due_91_plus_date { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_3160_never")]
        public string Most_recent_past_due_3160_never { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_6190_never")]
        public string Most_recent_past_due_6190_never { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_91_plus_never")]
        public string Most_recent_past_due_91_plus_never { get; set; }

        [XmlElement(ElementName = "contracts")]
        public Contracts Contracts { get; set; }

        [XmlAttribute(AttributeName = "number")]
        public string Number { get; set; }
    }

    [XmlRoot(ElementName = "member_lenders")]
    public class Member_lenders
    {
        [XmlElement(ElementName = "member_lender")]
        public Member_lender Member_lender { get; set; }
    }

    [XmlRoot(ElementName = "report_data")]
    public class Report_data
    {
        [XmlElement(ElementName = "generation_date_time")]
        public string Generation_date_time { get; set; }

        [XmlElement(ElementName = "paynet_id")]
        public string Paynet_id { get; set; }

        [XmlElement(ElementName = "report_id")]
        public string Report_id { get; set; }

        [XmlElement(ElementName = "generated_for_member")]
        public string Generated_for_member { get; set; }

        [XmlElement(ElementName = "name_address")]
        public Name_address Name_address { get; set; }

        [XmlElement(ElementName = "inquiries")]
        public Inquiries Inquiries { get; set; }

        [XmlElement(ElementName = "master_score")]
        public string Master_score { get; set; }

        [XmlElement(ElementName = "master_score_percentile")]
        public string Master_score_percentile { get; set; }

        [XmlElement(ElementName = "master_score_key_factor_1")]
        public string Master_score_key_factor_1 { get; set; }

        [XmlElement(ElementName = "master_score_key_factor_2")]
        public string Master_score_key_factor_2 { get; set; }

        [XmlElement(ElementName = "master_score_key_factor_3")]
        public string Master_score_key_factor_3 { get; set; }

        [XmlElement(ElementName = "primary_equipment_type_code")]
        public string Primary_equipment_type_code { get; set; }

        [XmlElement(ElementName = "primary_equipment_type_desc")]
        public string Primary_equipment_type_desc { get; set; }

        [XmlElement(ElementName = "oldest_contract_start_date")]
        public string Oldest_contract_start_date { get; set; }

        [XmlElement(ElementName = "last_activity_reported_date")]
        public string Last_activity_reported_date { get; set; }

        [XmlElement(ElementName = "open_contracts_cnt")]
        public string Open_contracts_cnt { get; set; }

        [XmlElement(ElementName = "closed_contracts_cnt")]
        public string Closed_contracts_cnt { get; set; }

        [XmlElement(ElementName = "orig_receivable_amt")]
        public string Orig_receivable_amt { get; set; }

        [XmlElement(ElementName = "high_credit_amt")]
        public string High_credit_amt { get; set; }

        [XmlElement(ElementName = "avg_high_credit_per_lender_amt")]
        public string Avg_high_credit_per_lender_amt { get; set; }

        [XmlElement(ElementName = "avg_orig_term_months")]
        public string Avg_orig_term_months { get; set; }

        [XmlElement(ElementName = "cur_bal_amt")]
        public string Cur_bal_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_amt_pct_of_high_credit")]
        public string Cur_bal_amt_pct_of_high_credit { get; set; }

        [XmlElement(ElementName = "cur_bal_amt_pct_of_orig_receivable")]
        public string Cur_bal_amt_pct_of_orig_receivable { get; set; }

        [XmlElement(ElementName = "monthly_payment_amt")]
        public string Monthly_payment_amt { get; set; }

        [XmlElement(ElementName = "annualized_scheduled_pmts")]
        public string Annualized_scheduled_pmts { get; set; }

        [XmlElement(ElementName = "loss_amt")]
        public string Loss_amt { get; set; }

        [XmlElement(ElementName = "scheduled_payments")]
        public string Scheduled_payments { get; set; }

        [XmlElement(ElementName = "past_due_3160_occurrences")]
        public string Past_due_3160_occurrences { get; set; }

        [XmlElement(ElementName = "past_due_6190_occurrences")]
        public string Past_due_6190_occurrences { get; set; }

        [XmlElement(ElementName = "past_due_91_plus_occurrences")]
        public string Past_due_91_plus_occurrences { get; set; }

        [XmlElement(ElementName = "past_due_31_plus_occurrences")]
        public string Past_due_31_plus_occurrences { get; set; }

        [XmlElement(ElementName = "cur_bal_cur_30_amt")]
        public string Cur_bal_cur_30_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_past_due_3160_amt")]
        public string Cur_bal_past_due_3160_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_past_due_6190_amt")]
        public string Cur_bal_past_due_6190_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_past_due_91_plus_amt")]
        public string Cur_bal_past_due_91_plus_amt { get; set; }

        [XmlElement(ElementName = "cur_bal_past_due_31_plus_amt")]
        public string Cur_bal_past_due_31_plus_amt { get; set; }

        [XmlElement(ElementName = "past_due_3160_amt")]
        public string Past_due_3160_amt { get; set; }

        [XmlElement(ElementName = "past_due_6190_amt")]
        public string Past_due_6190_amt { get; set; }

        [XmlElement(ElementName = "past_due_91_plus_amt")]
        public string Past_due_91_plus_amt { get; set; }

        [XmlElement(ElementName = "past_due_31_plus_amt")]
        public string Past_due_31_plus_amt { get; set; }

        [XmlElement(ElementName = "cur_30_amt_pct_of_cur_bal")]
        public string Cur_30_amt_pct_of_cur_bal { get; set; }

        [XmlElement(ElementName = "past_due_3160_amt_pct_of_cur_bal")]
        public string Past_due_3160_amt_pct_of_cur_bal { get; set; }

        [XmlElement(ElementName = "past_due_6190_amt_pct_of_cur_bal")]
        public string Past_due_6190_amt_pct_of_cur_bal { get; set; }

        [XmlElement(ElementName = "past_due_91_plus_amt_pct_of_cur_bal")]
        public string Past_due_91_plus_amt_pct_of_cur_bal { get; set; }

        [XmlElement(ElementName = "past_due_31_plus_amt_pct_of_cur_bal")]
        public string Past_due_31_plus_amt_pct_of_cur_bal { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_3160_date")]
        public string Most_recent_past_due_3160_date { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_6190_date")]
        public string Most_recent_past_due_6190_date { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_91_plus_date")]
        public string Most_recent_past_due_91_plus_date { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_3160_never")]
        public string Most_recent_past_due_3160_never { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_6190_never")]
        public string Most_recent_past_due_6190_never { get; set; }

        [XmlElement(ElementName = "most_recent_past_due_91_plus_never")]
        public string Most_recent_past_due_91_plus_never { get; set; }

        [XmlElement(ElementName = "most_recent_loss_amt_date")]
        public string Most_recent_loss_amt_date { get; set; }

        [XmlElement(ElementName = "most_recent_loss_status_date")]
        public string Most_recent_loss_status_date { get; set; }

        [XmlElement(ElementName = "recent_avg_days_past_due")]
        public string Recent_avg_days_past_due { get; set; }

        [XmlElement(ElementName = "recent_avg_days_past_due_unknown_ind")]
        public string Recent_avg_days_past_due_unknown_ind { get; set; }

        [XmlElement(ElementName = "hist_avg_days_past_due")]
        public string Hist_avg_days_past_due { get; set; }

        [XmlElement(ElementName = "hist_avg_days_past_due_unknown_ind")]
        public string Hist_avg_days_past_due_unknown_ind { get; set; }

        [XmlElement(ElementName = "contract_detail_included_cnt")]
        public string Contract_detail_included_cnt { get; set; }

        [XmlElement(ElementName = "member_lenders")]
        public Member_lenders Member_lenders { get; set; }

        [XmlElement(ElementName = "includes_others_contracts")]
        public string Includes_others_contracts { get; set; }

        [XmlElement(ElementName = "divergent_credit_quality")]
        public string Divergent_credit_quality { get; set; }

        [XmlElement(ElementName = "sic_codes")]
        public Sic_Codes Sic_Codes { get; set; }

        [XmlElement(ElementName = "legal_names")]
        public Legal_names Legal_names { get; set; }

        [XmlElement(ElementName = "legal_names_max")]
        public string Legal_names_max { get; set; }

        [XmlElement(ElementName = "legal_names_note")]
        public string Legal_names_note { get; set; }

        [XmlElement(ElementName = "business_background")]
        public Business_background Business_background { get; set; }

        [XmlElement(ElementName = "ucc_filings")]
        public Ucc_filings Ucc_filings { get; set; }
    }

    [XmlRoot(ElementName = "sic_code")]
    public class Sic_Code
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "sic_codes")]
    public class Sic_Codes
    {
        [XmlElement(ElementName = "sic_code")]
        public List<Sic_Code> Sic_code { get; set; }
    }

    [XmlRoot(ElementName = "legal_name")]
    public class Legal_name
    {
        [XmlElement(ElementName = "legal_corp_name")]
        public string Legal_corp_name { get; set; }
        [XmlElement(ElementName = "legal_address1")]
        public string Legal_address1 { get; set; }
        [XmlElement(ElementName = "legal_address2")]
        public string Legal_address2 { get; set; }
        [XmlElement(ElementName = "legal_city")]
        public string Legal_city { get; set; }
        [XmlElement(ElementName = "legal_state_code")]
        public string Legal_state_code { get; set; }
        [XmlElement(ElementName = "legal_postal_code")]
        public string Legal_postal_code { get; set; }
        [XmlElement(ElementName = "legal_entity_type")]
        public string Legal_entity_type { get; set; }
        [XmlElement(ElementName = "legal_filing_state")]
        public string Legal_filing_state { get; set; }
        [XmlElement(ElementName = "legal_status")]
        public string Legal_status { get; set; }
        [XmlElement(ElementName = "legal_date_incorporated")]
        public string Legal_date_incorporated { get; set; }
        [XmlElement(ElementName = "legal_last_yearly_report_date")]
        public string Legal_last_yearly_report_date { get; set; }
        [XmlElement(ElementName = "legal_filing_num")]
        public string Legal_filing_num { get; set; }
        [XmlElement(ElementName = "legal_officer_name1")]
        public string Legal_officer_name1 { get; set; }
        [XmlElement(ElementName = "legal_officer_title1")]
        public string Legal_officer_title1 { get; set; }
        [XmlElement(ElementName = "legal_officer_name2")]
        public string Legal_officer_name2 { get; set; }
        [XmlElement(ElementName = "legal_officer_title2")]
        public string Legal_officer_title2 { get; set; }
        [XmlElement(ElementName = "legal_officer_name3")]
        public string Legal_officer_name3 { get; set; }
        [XmlElement(ElementName = "legal_officer_title3")]
        public string Legal_officer_title3 { get; set; }
        [XmlElement(ElementName = "legal_officer_name4")]
        public string Legal_officer_name4 { get; set; }
        [XmlElement(ElementName = "legal_officer_title4")]
        public string Legal_officer_title4 { get; set; }
        [XmlElement(ElementName = "legal_officer_name5")]
        public string Legal_officer_name5 { get; set; }
        [XmlElement(ElementName = "legal_officer_title5")]
        public string Legal_officer_title5 { get; set; }
        [XmlElement(ElementName = "legal_taxid")]
        public string Legal_taxid { get; set; }
    }

    [XmlRoot(ElementName = "legal_names")]
    public class Legal_names
    {
        [XmlElement(ElementName = "legal_name")]
        public List<Legal_name> Legal_name { get; set; }
    }

    [XmlRoot(ElementName = "business_background_name_address")]
    public class Business_background_name_address
    {
        [XmlElement(ElementName = "primary_name")]
        public string Primary_name { get; set; }
        [XmlElement(ElementName = "address_1")]
        public string Address_1 { get; set; }
        [XmlElement(ElementName = "address_2")]
        public string Address_2 { get; set; }
        [XmlElement(ElementName = "city")]
        public string City { get; set; }
        [XmlElement(ElementName = "state_code")]
        public string State_code { get; set; }
        [XmlElement(ElementName = "postal_code")]
        public string Postal_code { get; set; }
        [XmlElement(ElementName = "country_code")]
        public string Country_code { get; set; }
        [XmlElement(ElementName = "telephone")]
        public string Telephone { get; set; }
        [XmlElement(ElementName = "fax")]
        public string Fax { get; set; }
        [XmlElement(ElementName = "tax_id")]
        public string Tax_id { get; set; }
    }

    [XmlRoot(ElementName = "manager")]
    public class Manager
    {
        [XmlAttribute(AttributeName = "title")]
        public string Title { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "managers")]
    public class Managers
    {
        [XmlElement(ElementName = "manager")]
        public Manager Manager { get; set; }
    }

    [XmlRoot(ElementName = "metro_area")]
    public class Metro_area
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "business_background")]
    public class Business_background
    {
        [XmlElement(ElementName = "business_background_name_address")]
        public Business_background_name_address Business_background_name_address { get; set; }
        [XmlElement(ElementName = "years_in_business")]
        public string Years_in_business { get; set; }
        [XmlElement(ElementName = "managers")]
        public Managers Managers { get; set; }
        [XmlElement(ElementName = "total_employees")]
        public string Total_employees { get; set; }
        [XmlElement(ElementName = "business_type")]
        public string Business_type { get; set; }
        [XmlElement(ElementName = "ownership_structure")]
        public string Ownership_structure { get; set; }
        [XmlElement(ElementName = "year_started")]
        public string Year_started { get; set; }
        [XmlElement(ElementName = "annual_sales")]
        public string Annual_sales { get; set; }
        [XmlElement(ElementName = "metro_area")]
        public Metro_area Metro_area { get; set; }
        [XmlElement(ElementName = "address_type")]
        public string Address_type { get; set; }
        [XmlElement(ElementName = "location_type")]
        public string Location_type { get; set; }
        [XmlElement(ElementName = "cur_bal_per_employee")]
        public string Cur_bal_per_employee { get; set; }
    }

    [XmlRoot(ElementName = "ucc_summary_count")]
    public class Ucc_summary_count
    {
        [XmlElement(ElementName = "date_range")]
        public string Date_range { get; set; }
        [XmlElement(ElementName = "total_filed")]
        public string Total_filed { get; set; }
        [XmlElement(ElementName = "cautionary")]
        public string Cautionary { get; set; }
        [XmlElement(ElementName = "released_and_terminated")]
        public string Released_and_terminated { get; set; }
        [XmlElement(ElementName = "continued")]
        public string Continued { get; set; }
        [XmlElement(ElementName = "amended_and_assigned")]
        public string Amended_and_assigned { get; set; }
    }

    [XmlRoot(ElementName = "ucc_summary_counts")]
    public class Ucc_summary_counts
    {
        [XmlElement(ElementName = "ucc_summary_count")]
        public List<Ucc_summary_count> Ucc_summary_count { get; set; }
    }

    [XmlRoot(ElementName = "ucc_summary_totals")]
    public class Ucc_summary_totals
    {
        [XmlElement(ElementName = "total_filed")]
        public string Total_filed { get; set; }
        [XmlElement(ElementName = "cautionary")]
        public string Cautionary { get; set; }
        [XmlElement(ElementName = "released_and_terminated")]
        public string Released_and_terminated { get; set; }
        [XmlElement(ElementName = "continued")]
        public string Continued { get; set; }
        [XmlElement(ElementName = "amended_and_assigned")]
        public string Amended_and_assigned { get; set; }
    }

    [XmlRoot(ElementName = "ucc_summary")]
    public class Ucc_summary
    {
        [XmlElement(ElementName = "ucc_summary_counts")]
        public Ucc_summary_counts Ucc_summary_counts { get; set; }
        [XmlElement(ElementName = "ucc_summary_totals")]
        public Ucc_summary_totals Ucc_summary_totals { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }

    [XmlRoot(ElementName = "collateral_items")]
    public class Collateral_items
    {
        [XmlElement(ElementName = "collateral")]
        public List<string> Collateral { get; set; }
    }

    [XmlRoot(ElementName = "ucc_filing")]
    public class Ucc_filing
    {
        [XmlElement(ElementName = "filing_date")]
        public string Filing_date { get; set; }
        [XmlElement(ElementName = "legal_action")]
        public string Legal_action { get; set; }
        [XmlElement(ElementName = "document_number")]
        public string Document_number { get; set; }
        [XmlElement(ElementName = "filing_location")]
        public string Filing_location { get; set; }
        [XmlElement(ElementName = "secured_party")]
        public string Secured_party { get; set; }
        [XmlElement(ElementName = "collateral_items")]
        public Collateral_items Collateral_items { get; set; }
        [XmlElement(ElementName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "address_1")]
        public string Address_1 { get; set; }
        [XmlElement(ElementName = "city")]
        public string City { get; set; }
        [XmlElement(ElementName = "state_code")]
        public string State_code { get; set; }
    }

    [XmlRoot(ElementName = "ucc_detail")]
    public class Ucc_detail
    {
        [XmlElement(ElementName = "ucc_filing")]
        public Ucc_filing Ucc_filing { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }

    [XmlRoot(ElementName = "ucc_filings")]
    public class Ucc_filings
    {
        [XmlElement(ElementName = "ucc_summary")]
        public Ucc_summary Ucc_summary { get; set; }
        [XmlElement(ElementName = "ucc_detail")]
        public Ucc_detail Ucc_detail { get; set; }
    }


    [XmlRoot(ElementName = "report")]
    public class GetReportResponse
    {
        [XmlElement(ElementName = "report_options")]
        public Report_options Report_options { get; set; }

        [XmlElement(ElementName = "report_data")]
        public Report_data Report_data { get; set; }

        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}