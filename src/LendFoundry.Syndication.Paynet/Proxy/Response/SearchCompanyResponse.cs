using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce {
    // [XmlRoot (ElementName = "companies")]
    // public partial class CompanySearchResponse {
    //     [XmlElement (ElementName = "company")]
    //     public Company[] Company { get; set; }
    // }

    // [XmlRoot (ElementName = "company")]
    // public partial class Company {
    //     [XmlElement ("paynet_id")]
    //     public string PayNetId { get; set; }

    //     [XmlElement ("name")]
    //     public string Name { get; set; }

    //     [XmlElement ("aliases")]
    //     public Aliases Aliases { get; set; }

    //     [XmlElement ("tax_id")]
    //     public string TaxId { get; set; }

    //     [XmlElement ("additional_taxids")]
    //     public AdditionalTaxids AdditionalTaxids { get; set; }

    //     [XmlElement ("street")]
    //     public string Street { get; set; }

    //     [XmlElement ("city")]
    //     public string City { get; set; }

    //     [XmlElement ("state_code")]
    //     public string StateCode { get; set; }

    //     [XmlElement ("postal_code")]
    //     public string PostalCode { get; set; }

    //     [XmlElement ("country_code")]
    //     public string CountryCode { get; set; }

    //     [XmlElement ("additional_addresses")]
    //     public AdditionalAddresses AdditionalAddresses { get; set; }

    //     [XmlElement ("telephone")]
    //     public string Telephone { get; set; }

    //     [XmlElement ("additional_phones")]
    //     public AdditionalPhones AdditionalPhones { get; set; }

    //     [XmlElement ("match_score")]
    //     public string MatchScore { get; set; }
    // }

    // [XmlRoot (ElementName = "additional_addresses")]
    // public partial class AdditionalAddresses {
    //     [XmlElement ("address")]
    //     public List<Addresses> Address { get; set; }
    // }

    // [XmlRoot (ElementName = "address")]
    // public partial class Addresses {
    //     [XmlElement ("street")]
    //     public string Street { get; set; }

    //     [XmlElement ("city")]
    //     public string City { get; set; }

    //     [XmlElement ("state")]
    //     public string State { get; set; }

    //     [XmlElement ("postal_code")]
    //     public string PostalCode { get; set; }

    //     [XmlElement ("country_code")]
    //     public string CountryCode { get; set; }
    // }

    // [XmlRoot (ElementName = "aliases")]
    // public partial class Aliases {
    //     [XmlElement (ElementName = "alias")]
    //     public string[] Alias { get; set; }
    // }

    // [XmlRoot (ElementName = "additional_taxids")]
    // public partial class AdditionalTaxids {
    //     [XmlElement (ElementName = "taxid")]
    //     public string[] taxid { get; set; }
    // }

    // [XmlRoot (ElementName = "additional_phones")]
    // public partial class AdditionalPhones {
    //     [XmlElement (ElementName = "phone")]
    //     public string[] Phone { get; set; }
    // }

    [XmlRoot (ElementName = "company")]
    public class Company {
        [XmlElement (ElementName = "paynet_id")]
        public string Paynet_id { get; set; }

        [XmlElement (ElementName = "name")]
        public string Name { get; set; }

        [XmlElement (ElementName = "tax_id")]
        public string Tax_id { get; set; }

        [XmlElement (ElementName = "street")]
        public string Street { get; set; }

        [XmlElement (ElementName = "city")]
        public string City { get; set; }

        [XmlElement (ElementName = "state_code")]
        public string State_code { get; set; }

        [XmlElement (ElementName = "postal_code")]
        public string Postal_code { get; set; }

        [XmlElement (ElementName = "country_code")]
        public string Country_code { get; set; }

        [XmlElement (ElementName = "telephone")]
        public string Telephone { get; set; }

        [XmlElement (ElementName = "match_score")]
        public string Match_score { get; set; }

        [XmlElement (ElementName = "aliases")]
        public Aliases Aliases { get; set; }

        [XmlElement (ElementName = "additional_taxids")]
        public Additional_taxids Additional_taxids { get; set; }

        [XmlElement (ElementName = "additional_phones")]
        public Additional_phones Additional_phones { get; set; }

        [XmlElement (ElementName = "additional_addresses")]
        public Additional_addresses Additional_addresses { get; set; }
    }

    [XmlRoot (ElementName = "aliases")]
    public class Aliases {
        [XmlElement (ElementName = "alias")]
        public List<string> Alias { get; set; }
    }

    [XmlRoot (ElementName = "additional_taxids")]
    public class Additional_taxids {
        [XmlElement (ElementName = "taxid")]
        public string Taxid { get; set; }
    }

    [XmlRoot (ElementName = "additional_phones")]
    public class Additional_phones {
        [XmlElement (ElementName = "phone")]
        public List<string> Phone { get; set; }
    }

    [XmlRoot (ElementName = "address")]
    public class Address {
        [XmlElement (ElementName = "street")]
        public string Street { get; set; }

        [XmlElement (ElementName = "city")]
        public string City { get; set; }

        [XmlElement (ElementName = "state")]
        public string State { get; set; }

        [XmlElement (ElementName = "postal_code")]
        public string Postal_code { get; set; }

        [XmlElement (ElementName = "country_code")]
        public string Country_code { get; set; }
    }

    [XmlRoot (ElementName = "additional_addresses")]
    public class Additional_addresses {
        [XmlElement (ElementName = "address")]
        public List<Address> Address { get; set; }
    }
     [XmlRoot (ElementName = "companies")]
    public class CompanySearchResponse {
        [XmlElement (ElementName = "company")]
        public List<Company> Company { get; set; }
    }   

    [XmlRoot (ElementName = "response")]
    public class Response {
        [XmlElement (ElementName = "companies")]
        public CompanySearchResponse Companies { get; set; }

        [XmlAttribute (AttributeName = "error_code")]
        public string Error_code { get; set; }

        [XmlAttribute (AttributeName = "version")]
        public string Version { get; set; }

        [XmlAttribute (AttributeName = "user_field")]
        public string User_field { get; set; }

        [XmlAttribute (AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

}