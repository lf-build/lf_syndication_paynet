﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet {
    public interface IPaynetConfiguration : IDependencyConfiguration {
        // ToDo : Check possible values once we have document
        string Baseurl { get; set; }
        string PaynetSearchUrl { get; set; }
        string PaynetReportUrl { get; set; }

        string UserName { get; set; }

        string Password { get; set; }

        string ProductOption { get; set; }
        string ProductType { get; set; }

        string ReportFormat { get; set; }

        string Score { get; set; }

        string UserField { get; set; }

        string Version { get; set; }
        bool UseProxy { get; set; }

        string NewMatchThreshold { get; set; }
        int CompanyNameLength { get; set; }
        int StateCodeLength { get; set; }
        string ConnectionString { get; set; }
    }
}