﻿using LendFoundry.SyndicationStore.Events;

namespace LendFoundry.Syndication.Paynet.Events
{
    public class PaynetGetCompanyReportRequestFail : SyndicationCalledEvent
    {
    }
}