﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Paynet
{
    [Serializable]
    public class PaynetException : Exception
    {
        #region Public Constructors

        public PaynetException()
        {
        }

        public PaynetException(string message) : this(null, message, null)
        {
        }

        public PaynetException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        public PaynetException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        public PaynetException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        #endregion Public Constructors

        #region Protected Constructors

        protected PaynetException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        #endregion Protected Constructors

        #region Public Properties

        public string ErrorCode { get; set; }

        #endregion Public Properties
    }
}