﻿using System.Xml.Serialization;

namespace LendFoundry.Syndication.Paynet.Request
{
    public class SearchCompanyRequest : ISearchCompanyRequest
    {
        public string CompanyName { get; set; }

        public string Alias { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string StateCode { get; set; }

        public string Phone { get; set; }

        public string TaxId { get; set; }

        public string UserField { get; set; }

        public string NewMatchThreshold { get; set; }
    }
}