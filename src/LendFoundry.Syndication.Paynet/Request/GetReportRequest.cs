﻿namespace LendFoundry.Syndication.Paynet.Request
{
    public class GetReportRequest : IGetReportRequest
    {
        public string PaynetId { get; set; }
        public string ProductType { get; set; }
        public string ReportFormat { get; set; }
        public string Score { get; set; }
        public string ProductOption { get; set; }
        public string UserField { get; set; }
    }
}