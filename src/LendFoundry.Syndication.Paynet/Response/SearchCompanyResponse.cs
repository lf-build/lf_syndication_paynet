using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Paynet.Response {
    public class SearchCompanyResponse : ISearchCompanyResponse {
        public SearchCompanyResponse () { }

        public SearchCompanyResponse (Syndication.Proxy.PayNetDirect.CompanySearchResponse response) {
            if (response != null && response.Body.CompanySearchResult != null) {
                var serializer = new XmlSerializer (typeof (CompanySearchResponse));
                var xml = XmlConvertor.Serialize (response, Encoding.UTF8);
                CompanySearchResponse responseResult = XmlConvertor.Deserialize<CompanySearchResponse> (xml.ToString ());
                CompanyList = responseResult.Company.Select (p => new Company (p)).ToList<ICompany> ();
            }
        }

        [JsonConverter (typeof (InterfaceListConverter<ICompany, Company>))]
        public List<ICompany> CompanyList { get; set; }
    }

}