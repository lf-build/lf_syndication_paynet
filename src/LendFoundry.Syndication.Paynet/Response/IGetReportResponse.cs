﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IGetReportResponse
    {
        object xmlField { get; set; }

        string htmlField { get; set; }

        List<IFile> filesField { get; set; }

        int errorCodeField { get; set; }

        string errorDescriptionField { get; set; }
    }
}