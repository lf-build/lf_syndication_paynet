using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface ISearchCompanyResponse
    {
        List<ICompany> CompanyList { get; set; }
    }
}