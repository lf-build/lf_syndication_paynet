﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class MemberLenders : IMemberLenders
    {
        public MemberLenders(Proxy.ReportResponse.Member_lender memberLeader)
        {
            if (memberLeader != null)
            {
                if (memberLeader.Primary_industry != null)
                    primary_industry = memberLeader.Primary_industry;
                if (memberLeader.As_of_date != null)
                    as_of_date = memberLeader.As_of_date;
                if (memberLeader.Orig_receivable_amt != null)
                    orig_receivable_amt = memberLeader.Orig_receivable_amt;
                if (memberLeader.High_credit_amt != null)
                    high_credit_amt = memberLeader.High_credit_amt;
                if (memberLeader.Cur_bal_amt != null)
                    cur_bal_amt = memberLeader.Cur_bal_amt;
                if (memberLeader.Cur_bal_amt_pct_of_high_credit != null)
                    cur_bal_amt_pct_of_high_credit = memberLeader.Cur_bal_amt_pct_of_high_credit;
                if (memberLeader.Cur_bal_amt_pct_of_orig_receivable != null)
                    cur_bal_amt_pct_of_orig_receivable = memberLeader.Cur_bal_amt_pct_of_orig_receivable;
                if (memberLeader.Monthly_payment_amt != null)
                    monthly_payment_amt = memberLeader.Monthly_payment_amt;
                if (memberLeader.Annualized_scheduled_pmts != null)
                    annualized_scheduled_pmts = memberLeader.Annualized_scheduled_pmts;
                if (memberLeader.Loss_amt != null)
                    loss_amt = memberLeader.Loss_amt;
                if (memberLeader.Past_due_3160_amt != null)
                    past_due_3160_amt = memberLeader.Past_due_3160_amt;
                if (memberLeader.Past_due_6190_amt != null)
                    past_due_6190_amt = memberLeader.Past_due_6190_amt;
                if (memberLeader.Past_due_91_plus_amt != null)
                    past_due_91_plus_amt = memberLeader.Past_due_91_plus_amt;
                if (memberLeader.Scheduled_payments != null)
                    scheduled_payments = memberLeader.Scheduled_payments;
                if (memberLeader.Past_due_3160_occurrences != null)
                    past_due_3160_occurrences = memberLeader.Past_due_3160_occurrences;
                if (memberLeader.Past_due_6190_occurrences != null)
                    past_due_6190_occurrences = memberLeader.Past_due_6190_occurrences;
                if (memberLeader.Past_due_91_plus_occurrences != null)
                    past_due_91_plus_occurrences = memberLeader.Past_due_91_plus_occurrences;
                if (memberLeader.Most_recent_past_due_91_plus_date != null)
                    most_recent_past_due_91_plus_date = memberLeader.Most_recent_past_due_91_plus_date.ToString();
                if (memberLeader.Most_recent_past_due_3160_never != null)
                    most_recent_past_due_3160_never = memberLeader.Most_recent_past_due_3160_never;
                if (memberLeader.Most_recent_past_due_6190_never != null)
                    most_recent_past_due_6190_never = memberLeader.Most_recent_past_due_6190_never;
                if (memberLeader.Most_recent_past_due_91_plus_never != null)
                    most_recent_past_due_91_plus_never = memberLeader.Most_recent_past_due_91_plus_never;
                if (memberLeader.Contracts != null)
                    Contracts = memberLeader.Contracts.Contract.Select(p => new Contracts(p))
                            .ToList<IContracts>();
            }
        }

        public string primary_industry { get; set; }

        public string as_of_date { get; set; }

        public string orig_receivable_amt { get; set; }

        public string high_credit_amt { get; set; }

        public string cur_bal_amt { get; set; }

        public string cur_bal_amt_pct_of_high_credit { get; set; }

        public string cur_bal_amt_pct_of_orig_receivable { get; set; }

        public string monthly_payment_amt { get; set; }

        public string annualized_scheduled_pmts { get; set; }

        public string loss_amt { get; set; }

        public string past_due_3160_amt { get; set; }

        public string past_due_6190_amt { get; set; }

        public string past_due_91_plus_amt { get; set; }

        public string scheduled_payments { get; set; }

        public string past_due_3160_occurrences { get; set; }

        public string past_due_6190_occurrences { get; set; }

        public string past_due_91_plus_occurrences { get; set; }

        public string most_recent_past_due_3160_date { get; set; }

        public string most_recent_past_due_6190_date { get; set; }

        public string most_recent_past_due_91_plus_date { get; set; }

        public string most_recent_past_due_3160_never { get; set; }

        public string most_recent_past_due_6190_never { get; set; }

        public string most_recent_past_due_91_plus_never { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IContracts, Contracts>))]
        public List<IContracts> Contracts { get; set; }
    }
}