﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public class UccSummaryCount: IUccSummaryCount
    {
        public UccSummaryCount(Proxy.ReportResponse.Ucc_summary_count uccSummaryCount)
        {
            if (null != uccSummaryCount)
            {
                DateRange = uccSummaryCount.Date_range;
                TotalFiled = uccSummaryCount.Total_filed;
                Cautionary = uccSummaryCount.Cautionary;
                ReleasedAndTerminated = uccSummaryCount.Released_and_terminated;
                Continued = uccSummaryCount.Continued;
                AmendedAndAssigned = uccSummaryCount.Amended_and_assigned;
            }

        }
        public string DateRange { get; set; }
        public string TotalFiled { get; set; }
        public string Cautionary { get; set; }
        public string ReleasedAndTerminated { get; set; }
        public string Continued { get; set; }
        public string AmendedAndAssigned { get; set; }
    }
}
