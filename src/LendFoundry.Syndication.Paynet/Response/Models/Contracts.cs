﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class Contracts : IContracts
    {
        public Contracts(Proxy.ReportResponse.Contract responseContact)
        {
            if (responseContact != null)
            {
                if (responseContact.Name_address != null)
                    NameAddress = new NameAddress(responseContact.Name_address);
                if (responseContact.Contract_type != null)
                    contract_type = responseContact.Contract_type;
                if (responseContact.Equipment_type != null)
                    equipment_type = responseContact.Equipment_type;
                if (responseContact.Guarantor_code != null)
                    guarantor_code = responseContact.Guarantor_code;
                if (responseContact.Term != null)
                    term = responseContact.Term;
                if (responseContact.Payment_amt != null)
                    payment_amt = responseContact.Payment_amt;
                if (responseContact.Payment_freq != null)
                    payment_freq = responseContact.Payment_freq;
                if (responseContact.Orig_receivable_amt != null)
                    orig_receivable_amt = responseContact.Orig_receivable_amt;
                if (responseContact.Cur_bal_amt != null)
                    cur_bal_amt = responseContact.Cur_bal_amt;
                if (responseContact.Cur_bal_pct_of_orig_receivable != null)
                    cur_bal_pct_of_orig_receivable = responseContact.Cur_bal_pct_of_orig_receivable.ToString();
                if (responseContact.Past_due_3160_amt != null)
                    past_due_3160_amt = responseContact.Past_due_3160_amt;
                if (responseContact.Past_due_6190_amt != null)
                    past_due_6190_amt = responseContact.Past_due_6190_amt;
                if (responseContact.Past_due_91_plus_amt != null)
                    past_due_91_plus_amt = responseContact.Past_due_91_plus_amt;
                if (responseContact.Scheduled_payments != null)
                    scheduled_payments = responseContact.Scheduled_payments;
                if (responseContact.Past_due_3160_occurrences != null)
                    past_due_3160_occurrences = responseContact.Past_due_3160_occurrences;
                if (responseContact.Past_due_91_plus_occurrences != null)
                    past_due_91_plus_occurrences = responseContact.Past_due_91_plus_occurrences;
                if (responseContact.Days_past_due != null)
                    days_past_due = responseContact.Days_past_due.ToString();
                if (responseContact.Avg_days_past_due != null)
                    avg_days_past_due = responseContact.Avg_days_past_due;
                if (responseContact.Max_days_past_due_low != null)
                    max_days_past_due_low = responseContact.Max_days_past_due_low;
                if (responseContact.Max_days_past_due_high != null)
                    max_days_past_due_high = responseContact.Max_days_past_due_high.ToString();
                if (responseContact.Loss_amt != null)
                    loss_amt = responseContact.Loss_amt;
                if (responseContact.Renewal_date != null)
                    renewal_date = responseContact.Renewal_date.ToString();
                if (responseContact.Closed_date != null)
                    closed_date = responseContact.Closed_date;
                if (responseContact.As_of_date != null)
                    as_of_date = responseContact.As_of_date;
                if (responseContact.Start_date != null)
                    start_date = responseContact.Start_date;
                if (responseContact.Last_payment_date != null)
                    last_payment_date = responseContact.Last_payment_date;
                if (responseContact.Next_payment_due_date != null)
                    next_payment_due_date = responseContact.Next_payment_due_date.ToString();
                if (responseContact.Max_past_due_date != null)
                    max_past_due_date = responseContact.Max_past_due_date.ToString();
                if (responseContact.Loss_status != null)
                    loss_status = responseContact.Loss_status.ToString();
                if (responseContact.Contract_open_ind != null)
                    contract_open_ind = responseContact.Contract_open_ind;
                if (responseContact.Transferred_to_other_lender != null)
                    transferred_to_other_lender = responseContact.Transferred_to_other_lender;
                if (responseContact.Transferred_from_other_lender != null)
                    transferred_from_other_lender = responseContact.Transferred_from_other_lender;
                if (responseContact.First_bal_date != null)
                    first_bal_date = responseContact.First_bal_date;
            }
        }

        [JsonConverter(typeof(InterfaceConverter<INameAddress, NameAddress>))]
        public INameAddress NameAddress { get; set; }

        public string contract_type { get; set; }

        public string equipment_type { get; set; }

        public string guarantor_code { get; set; }

        public string term { get; set; }

        public string payment_amt { get; set; }

        public string payment_freq { get; set; }

        public string orig_receivable_amt { get; set; }

        public string cur_bal_amt { get; set; }

        public string cur_bal_pct_of_orig_receivable { get; set; }

        public string past_due_3160_amt { get; set; }

        public string past_due_6190_amt { get; set; }

        public string past_due_91_plus_amt { get; set; }

        public string scheduled_payments { get; set; }

        public string past_due_3160_occurrences { get; set; }

        public string past_due_91_plus_occurrences { get; set; }

        public string days_past_due { get; set; }

        public string avg_days_past_due { get; set; }

        public string max_days_past_due_low { get; set; }

        public string max_days_past_due_high { get; set; }

        public string loss_amt { get; set; }

        public string renewal_date { get; set; }

        public string closed_date { get; set; }

        public string as_of_date { get; set; }

        public string start_date { get; set; }

        public string last_payment_date { get; set; }

        public string next_payment_due_date { get; set; }

        public string max_past_due_date { get; set; }

        public string loss_status { get; set; }

        public string contract_open_ind { get; set; }

        public string transferred_to_other_lender { get; set; }

        public string transferred_from_other_lender { get; set; }

        public string first_bal_date { get; set; }
    }
}