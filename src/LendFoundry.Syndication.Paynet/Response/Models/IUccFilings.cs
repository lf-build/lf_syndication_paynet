﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IUccFilings
    {
        IUccSummary UccSummary { get; set; }
        IUccDetail UccDetail { get; set; }
    }
}
