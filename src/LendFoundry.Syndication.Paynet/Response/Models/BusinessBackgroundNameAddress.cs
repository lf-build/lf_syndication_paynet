﻿
namespace LendFoundry.Syndication.Paynet.Response
{
    public class BusinessBackgroundNameAddress : IBusinessBackgroundNameAddress
    {
        public BusinessBackgroundNameAddress(Proxy.ReportResponse.Business_background_name_address businessAddress)
        {
            if (null != businessAddress)
            {
                PrimaryName = businessAddress.Primary_name;
                Address1 = businessAddress.Address_1;
                Address2 = businessAddress.Address_2;
                City = businessAddress.City;
                StateCode = businessAddress.State_code;
                PostalCode = businessAddress.Postal_code;
                CountryCode = businessAddress.Country_code;
                Telephone = businessAddress.Telephone;
                Fax = businessAddress.Fax;
                TaxId = businessAddress.Tax_id;
            }
        }
        public string PrimaryName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string TaxId { get; set; }
    }
}
