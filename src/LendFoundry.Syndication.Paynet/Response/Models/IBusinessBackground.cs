﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IBusinessBackground
    {
        IBusinessBackgroundNameAddress Businessbackgroundnameaddress { get; set; }
        string YearsInBusiness { get; set; }
        IManager Managers { get; set; }
        string TotalEmployees { get; set; }
        string BusinessType { get; set; }
        string OwnershipStructure { get; set; }
        string YearStarted { get; set; }
        string AnnualSales { get; set; }
        IMetroArea MetroArea { get; set; }
        string AddressType { get; set; }
        string LocationType { get; set; }
        string CurBalPerEmployee { get; set; }
    }
}
