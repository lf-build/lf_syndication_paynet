﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response {
    public class Addresses : IAddresses {
        public Addresses () { }

        public Addresses (Proxy.CompanySearchResponce.Address address) {
            if (address != null) {
                if (address.Street != null)
                    Street = address.Street;
                if (address.City != null)

                    City = address.City;
                if (address.State != null)

                    State = address.State;
                if (address.Postal_code != null)

                    PostalCode = address.Postal_code;
                if (address.Country_code != null)

                    CountryCode = address.Country_code;
            }
        }

        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
    }
}