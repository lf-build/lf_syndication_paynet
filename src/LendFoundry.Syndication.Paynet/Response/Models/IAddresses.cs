﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IAddresses
    {
        string Street { get; set; }
        string City { get; set; }

        string State { get; set; }
        string PostalCode { get; set; }
        string CountryCode { get; set; }
    }
}