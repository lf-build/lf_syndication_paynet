﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public class LegalName : ILegalName
    {
        public LegalName(Proxy.ReportResponse.Legal_name legalName)
        {
            if (null != legalName)
            {
                LegalCorpName = legalName.Legal_corp_name.ReplaceIfNotNull();
                LegalAddress1 = legalName.Legal_address1.ReplaceIfNotNull();
                LegalAddress2 = legalName.Legal_address2.ReplaceIfNotNull();
                LegalCity = legalName.Legal_city.ReplaceIfNotNull();
                LegalStateCode = legalName.Legal_state_code.ReplaceIfNotNull();
                LegalPostalCode = legalName.Legal_postal_code.ReplaceIfNotNull();
                LegalEntityType = legalName.Legal_entity_type.ReplaceIfNotNull();
                LegalFilingState = legalName.Legal_filing_state.ReplaceIfNotNull();
                LegalStatus = legalName.Legal_status.ReplaceIfNotNull();
                LegalDateIncorporated = legalName.Legal_date_incorporated.ReplaceIfNotNull();
                LegalLastYearlyReportDate = legalName.Legal_last_yearly_report_date.ReplaceIfNotNull();
                LegalFilingNum = legalName.Legal_filing_num.ReplaceIfNotNull();
                LegalOfficerName1 = legalName.Legal_officer_name1.ReplaceIfNotNull();
                LegalOfficerTitle1 = legalName.Legal_officer_title1.ReplaceIfNotNull();
                LegalOfficerName2 = legalName.Legal_officer_name2.ReplaceIfNotNull();
                LegalOfficerTitle2 = legalName.Legal_officer_title2.ReplaceIfNotNull();
                LegalOfficerName3 = legalName.Legal_officer_name3.ReplaceIfNotNull();
                LegalOfficerTitle3 = legalName.Legal_officer_title3.ReplaceIfNotNull();
                LegalOfficerName4 = legalName.Legal_officer_name4.ReplaceIfNotNull();
                LegalOfficerTitle4 = legalName.Legal_officer_title4.ReplaceIfNotNull();
                LegalOfficerName5 = legalName.Legal_officer_name5.ReplaceIfNotNull();
                LegalOfficerTitle5 = legalName.Legal_officer_title5.ReplaceIfNotNull();
                LegalTaxid = legalName.Legal_taxid;
            }

        }
        public string LegalCorpName { get; set; }
        public string LegalAddress1 { get; set; }
        public string LegalAddress2 { get; set; }
        public string LegalCity { get; set; }
        public string LegalStateCode { get; set; }
        public string LegalPostalCode { get; set; }
        public string LegalEntityType { get; set; }
        public string LegalFilingState { get; set; }
        public string LegalStatus { get; set; }
        public string LegalDateIncorporated { get; set; }
        public string LegalLastYearlyReportDate { get; set; }
        public string LegalFilingNum { get; set; }
        public string LegalOfficerName1 { get; set; }
        public string LegalOfficerTitle1 { get; set; }
        public string LegalOfficerName2 { get; set; }
        public string LegalOfficerTitle2 { get; set; }
        public string LegalOfficerName3 { get; set; }
        public string LegalOfficerTitle3 { get; set; }
        public string LegalOfficerName4 { get; set; }
        public string LegalOfficerTitle4 { get; set; }
        public string LegalOfficerName5 { get; set; }
        public string LegalOfficerTitle5 { get; set; }
        public string LegalTaxid { get; set; }
    }
}
