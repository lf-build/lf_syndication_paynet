﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IUccSummary
    {
         List<IUccSummaryCount> UccSummaryCounts { get; set; }
         IUccSummaryTotals UccSummaryTotals { get; set; }
         string Xsi { get; set; }
    }
}
