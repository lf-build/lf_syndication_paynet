﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IGetReportResult
    {
        IReportData ReportData { get; set; }

        List<IReportOption> ReportOption { get; set; }
    }
}