﻿namespace LendFoundry.Syndication.Paynet.Response
{
    public interface INameAddress
    {
        string primary_name { get; set; }

        string address_1 { get; set; }

        string address_2 { get; set; }

        string city { get; set; }

        string state_code { get; set; }

        string postal_code { get; set; }

        string telephone { get; set; }

        string fax { get; set; }

        string tax_id { get; set; }

        OtherNames OtherNames { get; set; }

    }
}