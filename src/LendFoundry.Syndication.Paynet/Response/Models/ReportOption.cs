﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class ReportOption : IReportOption
    {
        public ReportOption(Proxy.ReportResponse.Report_option reportOptions)
        {
            if (reportOptions != null)
            {
                report_option = reportOptions.Text;
            }
        }

        public string report_option { get; set; }
    }
}