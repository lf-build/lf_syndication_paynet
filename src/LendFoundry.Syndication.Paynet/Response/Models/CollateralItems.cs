﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class CollateralItems : ICollateralItems
    {
        public CollateralItems(Proxy.ReportResponse.Collateral_items collateralItems)
        {
            if (null != collateralItems)
            {
                Collateral = collateralItems.Collateral;
            }

        }
        public List<string> Collateral { get; set; }
    }
}
