﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface ISicCode
    {
        string Code { get; set; }
        string Text { get; set; }
    }
}
