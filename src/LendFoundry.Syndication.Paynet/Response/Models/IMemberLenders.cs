﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IMemberLenders
    {
        string primary_industry { get; set; }

        string as_of_date { get; set; }

        string orig_receivable_amt { get; set; }

        string high_credit_amt { get; set; }

        string cur_bal_amt { get; set; }

        string cur_bal_amt_pct_of_high_credit { get; set; }

        string cur_bal_amt_pct_of_orig_receivable { get; set; }

        string monthly_payment_amt { get; set; }

        string annualized_scheduled_pmts { get; set; }

        string loss_amt { get; set; }

        string past_due_3160_amt { get; set; }

        string past_due_6190_amt { get; set; }

        string past_due_91_plus_amt { get; set; }

        string scheduled_payments { get; set; }

        string past_due_3160_occurrences { get; set; }

        string past_due_6190_occurrences { get; set; }

        string past_due_91_plus_occurrences { get; set; }

        string most_recent_past_due_3160_date { get; set; }

        string most_recent_past_due_6190_date { get; set; }

        string most_recent_past_due_91_plus_date { get; set; }

        string most_recent_past_due_3160_never { get; set; }

        string most_recent_past_due_6190_never { get; set; }

        string most_recent_past_due_91_plus_never { get; set; }

        List<IContracts> Contracts { get; set; }
    }
}