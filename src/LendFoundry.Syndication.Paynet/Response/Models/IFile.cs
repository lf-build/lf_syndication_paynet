﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IFile
    {
        byte[] bytesField { get; set; }

        string contentTypeField { get; set; }

        string fileNameField { get; set; }

        int fileSizeField { get; set; }
    }
}