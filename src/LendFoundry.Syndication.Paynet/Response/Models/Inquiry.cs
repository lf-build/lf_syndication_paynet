﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class Inquiry : IInquiry
    {
        public Inquiry(Proxy.ReportResponse.Inquiry inquiries)
        {
            if (inquiries != null)
            {
                inquiry = inquiries.Text.ReplaceIfNotNull();
            }
        }

        public string inquiry { get; set; }
    }
}