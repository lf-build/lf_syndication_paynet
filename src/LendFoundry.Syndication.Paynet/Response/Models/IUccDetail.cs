﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IUccDetail
    {
        IUccFiling UccFiling { get; set; }
        string Xsi { get; set; }
    }
}
