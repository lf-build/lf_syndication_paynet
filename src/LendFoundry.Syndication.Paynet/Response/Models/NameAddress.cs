﻿namespace LendFoundry.Syndication.Paynet.Response
{
    public class NameAddress : INameAddress
    {
        public NameAddress(Proxy.ReportResponse.Name_address nameAddress)
        {
            if (nameAddress != null)
            {
                if (nameAddress.Primary_name != null)
                    primary_name = nameAddress.Primary_name.ReplaceIfNotNull();
                if (nameAddress.Address_1 != null)
                    address_1 = nameAddress.Address_1.ReplaceIfNotNull();
                if (nameAddress.Address_2 != null)
                    address_2 = nameAddress.Address_2.ReplaceIfNotNull();
                if (nameAddress.City != null)
                    city = nameAddress.City.ReplaceIfNotNull();
                if (nameAddress.State_code != null)
                    state_code = nameAddress.State_code.ReplaceIfNotNull();
                if (nameAddress.Postal_code != null)
                    postal_code = nameAddress.Postal_code.ReplaceIfNotNull();
                if (nameAddress.Telephone != null)
                    telephone = nameAddress.Telephone.ReplaceIfNotNull();
                if (nameAddress.Fax != null)
                    fax = nameAddress.Fax.ReplaceIfNotNull();
                if (nameAddress.Tax_id != null)
                    tax_id = nameAddress.Tax_id.ReplaceIfNotNull();
                if (nameAddress.Other_names != null)
                    OtherNames = new OtherNames(nameAddress.Other_names);
            }
        }

        public string primary_name { get; set; }

        public string address_1 { get; set; }

        public string address_2 { get; set; }

        public string city { get; set; }

        public string state_code { get; set; }

        public string postal_code { get; set; }

        public string telephone { get; set; }

        public string fax { get; set; }

        public string tax_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public OtherNames OtherNames { get; set; }
    }
}