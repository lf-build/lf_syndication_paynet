﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class GetReportResult : IGetReportResult
    {
        public GetReportResult()
        {
        }

        public GetReportResult(Proxy.ReportResponse.GetReportResponse response)
        {
            if (response != null)
            {
                if (response.Report_data != null)
                    ReportData = new ReportData(response.Report_data);
                if (response.Report_options != null)
                    ReportOption = response.Report_options.Report_option.Select(p => new ReportOption(p)).ToList<IReportOption>();
            }
        }

        [JsonConverter(typeof(InterfaceConverter<IReportData, ReportData>))]
        public IReportData ReportData { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IReportOption, ReportOption>))]
        public List<IReportOption> ReportOption { get; set; }
    }
}