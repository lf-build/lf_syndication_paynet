﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IBusinessBackgroundNameAddress
    {
        string PrimaryName { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string StateCode { get; set; }
        string PostalCode { get; set; }
        string CountryCode { get; set; }
        string Telephone { get; set; }
        string Fax { get; set; }
        string TaxId { get; set; }
    }
}
