﻿using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Paynet.Response {
    public class Company : ICompany {
        public Company (Proxy.CompanySearchResponce.Company response) {
            if (response != null) {
                if (response.Paynet_id != null)
                    PayNetId = response.Paynet_id;
                if (response.Name != null)

                    Name = response.Name;
                if (response.Aliases != null)

                    Aliasses = response.Aliases.Alias;
                if (response.Tax_id != null)

                    TaxId = response.Tax_id;
                if (response.Street != null)

                    Street = response.Street;
                if (response.City != null)

                    City = response.City;
                if (response.State_code != null)

                    StateCode = response.State_code;
                if (response.Postal_code != null)

                    PostalCode = response.Postal_code;
                if (response.Country_code != null)

                    CountryCode = response.Country_code;
                if (response.Telephone != null)

                    Telephone = response.Telephone;
                if (response.Additional_phones != null)
                    AdditionalPhones = response.Additional_phones.Phone;
                if (response.Match_score != null)

                    MatchScore = response.Match_score;
                if (response.Additional_addresses != null)
                    AdditionalAddresses =
                    response.Additional_addresses.Address.Select (p => new Addresses (p))
                    .ToList<IAddresses> ();
            }
        }

        public string PayNetId { get; set; }
        public string Name { get; set; }

        public List<string> Aliasses { get; set; }

        public string TaxId { get; set; }

        public string Street { get; set; }
        public string City { get; set; }

        public string StateCode { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IAddresses, Addresses>))]
        public List<IAddresses> AdditionalAddresses { get; set; }

        public string Telephone { get; set; }

        public List<string> AdditionalPhones { get; set; }

        public string MatchScore { get; set; }
    }
}