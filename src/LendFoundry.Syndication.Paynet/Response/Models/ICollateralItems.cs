﻿

using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface ICollateralItems
    {
         List<string> Collateral { get; set; }
    }
}
