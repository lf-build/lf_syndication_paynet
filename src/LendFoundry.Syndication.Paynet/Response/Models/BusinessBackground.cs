﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public class BusinessBackground : IBusinessBackground
    {
        public BusinessBackground(Proxy.ReportResponse.Business_background businessBackGround)
        {
            if (null != businessBackGround)
            {
                YearsInBusiness = businessBackGround.Years_in_business;
                TotalEmployees = businessBackGround.Total_employees;
                BusinessType = businessBackGround.Business_type;
                OwnershipStructure = businessBackGround.Ownership_structure;
                YearStarted = businessBackGround.Year_started;
                AnnualSales = businessBackGround.Annual_sales;
                AddressType = businessBackGround.Address_type;
                LocationType = businessBackGround.Location_type;
                CurBalPerEmployee = businessBackGround.Cur_bal_per_employee;
                if (businessBackGround.Business_background_name_address != null)
                    Businessbackgroundnameaddress = new BusinessBackgroundNameAddress(businessBackGround.Business_background_name_address);
                if (businessBackGround.Metro_area != null)
                    MetroArea = new MetroArea(businessBackGround.Metro_area);
                if (businessBackGround.Managers != null)
                    Managers = new Manager(businessBackGround.Managers.Manager);
            }

        }

        public IBusinessBackgroundNameAddress Businessbackgroundnameaddress { get; set; }
        public string YearsInBusiness { get; set; }
        public IManager Managers { get; set; }
        public string TotalEmployees { get; set; }
        public string BusinessType { get; set; }
        public string OwnershipStructure { get; set; }
        public string YearStarted { get; set; }
        public string AnnualSales { get; set; }
        public IMetroArea MetroArea { get; set; }
        public string AddressType { get; set; }
        public string LocationType { get; set; }
        public string CurBalPerEmployee { get; set; }
    }
}
