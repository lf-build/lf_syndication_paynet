﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class ReportData : IReportData
    {
        public ReportData()
        {
        }

        public ReportData(Proxy.ReportResponse.Report_data reportData)
        {
            if (reportData != null)
            {
                if (reportData.Generation_date_time != null)
                    GenerationDateTime = reportData.Generation_date_time;
                if (!string.IsNullOrWhiteSpace(reportData.Generated_for_member))
                    GeneratedForMember = reportData.Generated_for_member.ReplaceIfNotNull();
                if (reportData.Paynet_id != null)
                    PaynetId = reportData.Paynet_id.ReplaceIfNotNull(); 
                if (reportData.Report_id != null)
                    ReportId = reportData.Report_id.ReplaceIfNotNull();
                if (reportData.Name_address != null)
                    NameAddress = new NameAddress(reportData.Name_address);
                if (reportData.Inquiries != null)
                    inquiries = reportData.Inquiries.Inquiry.Select(p => new Inquiry(p)).ToList<IInquiry>();
                if (reportData.Master_score != null)
                    MasterScore = reportData.Master_score.ReplaceIfNotNull();
                if (reportData.Master_score_percentile != null)
                    MasterScorePercentile = reportData.Master_score_percentile.ReplaceIfNotNull();
                if (reportData.Master_score_key_factor_1 != null)
                    MasterScoreKeyFactor_1 = reportData.Master_score_key_factor_1.ReplaceIfNotNull();
                if (reportData.Master_score_key_factor_2 != null)
                    MasterScoreKeyFactor_2 = reportData.Master_score_key_factor_2.ReplaceIfNotNull();
                if (reportData.Master_score_key_factor_3 != null)
                    MasterScoreKeyFactor_3 = reportData.Master_score_key_factor_3.ReplaceIfNotNull();
                if (reportData.Primary_equipment_type_code != null)
                    PrimaryEquipmentTypeCode = reportData.Primary_equipment_type_code.ReplaceIfNotNull();
                if (reportData.Primary_equipment_type_desc != null)
                    PrimaryEquipmentTypeDesc = reportData.Primary_equipment_type_desc.ReplaceIfNotNull();
                if (reportData.Oldest_contract_start_date != null)
                    OldestContractStartDate = reportData.Oldest_contract_start_date.ReplaceIfNotNull();
                if (reportData.Last_activity_reported_date != null)
                    LastActivityReportedDate = reportData.Last_activity_reported_date;
                if (reportData.Open_contracts_cnt != null)
                    OpenContractsCnt = reportData.Open_contracts_cnt.ReplaceIfNotNull();
                if (reportData.Closed_contracts_cnt != null)
                    ClosedContractsCnt = reportData.Closed_contracts_cnt.ReplaceIfNotNull();
                if (reportData.Orig_receivable_amt != null)
                    OrigReceivableAmt = reportData.Orig_receivable_amt.ReplaceIfNotNull();
                if (reportData.High_credit_amt != null)
                    HighCreditAmt = reportData.High_credit_amt.ReplaceIfNotNull();
                if (reportData.Avg_high_credit_per_lender_amt != null)
                    AvgHighCreditPerLenderAmt = reportData.Avg_high_credit_per_lender_amt.ReplaceIfNotNull();
                if (reportData.Avg_orig_term_months != null)
                    AvgOrigTermMonths = reportData.Avg_orig_term_months.ReplaceIfNotNull();
                if (reportData.Cur_bal_amt != null)
                    CurBalAmt = reportData.Cur_bal_amt.ReplaceIfNotNull();
                if (reportData.Cur_bal_amt_pct_of_high_credit != null)
                    CurBalAmtPctOfHighCredit = reportData.Cur_bal_amt_pct_of_high_credit.ReplaceIfNotNull();
                if (reportData.Cur_bal_amt_pct_of_orig_receivable != null)
                    CurBalAmtPctOfOrigReceivable = reportData.Cur_bal_amt_pct_of_orig_receivable.ReplaceIfNotNull();
                if (reportData.Monthly_payment_amt != null)
                    MonthlyPaymentAmt = reportData.Monthly_payment_amt;
                if (reportData.Annualized_scheduled_pmts != null)
                    annualized_scheduled_pmts = reportData.Annualized_scheduled_pmts;
                if (reportData.Loss_amt != null)
                    LossAmt = reportData.Loss_amt;
                if (reportData.Scheduled_payments != null)
                    ScheduledPayments = reportData.Scheduled_payments;
                if (reportData.Past_due_3160_occurrences != null)
                    PastDue_3160_Occurrences = reportData.Past_due_3160_occurrences;
                if (reportData.Past_due_6190_occurrences != null)
                    PastDue_6190_Occurrences = reportData.Past_due_6190_occurrences;
                if (reportData.Past_due_91_plus_occurrences != null)
                    PastDue_91_Plus_Occurrences = reportData.Past_due_91_plus_occurrences;
                if (reportData.Past_due_31_plus_occurrences != null)
                    Past_Due_31_Plus_Occurrences = reportData.Past_due_31_plus_occurrences;
                if (reportData.Cur_bal_cur_30_amt != null)
                    Cur_Bal_Cur_30_Amt = reportData.Cur_bal_cur_30_amt;
                if (reportData.Cur_bal_past_due_3160_amt != null)
                    CurBalPastDue_3160_Amt = reportData.Cur_bal_past_due_3160_amt;
                if (reportData.Cur_bal_past_due_6190_amt != null)
                    CurBalPastDue_6190_Amt = reportData.Cur_bal_past_due_6190_amt;
                if (reportData.Cur_bal_past_due_91_plus_amt != null)
                    CurBalPastDue_91_Plus_Amt = reportData.Cur_bal_past_due_91_plus_amt;
                if (reportData.Cur_bal_past_due_31_plus_amt != null)
                    CurBalPastDue_31_Plus_Amt = reportData.Cur_bal_past_due_31_plus_amt;
                if (reportData.Past_due_3160_amt != null)
                    PastDue_3160_Amt = reportData.Past_due_3160_amt;
                if (reportData.Past_due_6190_amt != null)
                    PastDue_6190_Amt = reportData.Past_due_6190_amt;
                if (reportData.Past_due_91_plus_amt != null)
                    PastDue_91_Plus_Amt = reportData.Past_due_91_plus_amt;
                if (reportData.Past_due_31_plus_amt != null)
                    PastDue_31_Plus_Amt = reportData.Past_due_31_plus_amt;
                if (reportData.Cur_30_amt_pct_of_cur_bal != null)
                    Cur_30_AmtPctOfCurBal = reportData.Cur_30_amt_pct_of_cur_bal;
                if (reportData.Past_due_3160_amt_pct_of_cur_bal != null)
                    PastDue_3160_Amt_Pct_Of_Cur_Bal = reportData.Past_due_3160_amt_pct_of_cur_bal.ToString();
                if (reportData.Past_due_6190_amt_pct_of_cur_bal != null)
                    Past_Due_6190_Amt_Pct_Of_Cur_Bal = reportData.Past_due_6190_amt_pct_of_cur_bal.ToString();
                if (reportData.Past_due_91_plus_amt_pct_of_cur_bal != null)
                    Past_Due_91_Plus_Amt_Pct_Of_Cur_Bal = reportData.Past_due_91_plus_amt_pct_of_cur_bal.ToString();
                if (reportData.Past_due_31_plus_amt_pct_of_cur_bal != null)
                    Past_Due_31_Plus_Amt_Pct_Of_Cur_Bal = reportData.Past_due_31_plus_amt_pct_of_cur_bal;
                if (reportData.Most_recent_past_due_3160_date != null)
                    Most_Recent_Past_Due_3160_Date = reportData.Most_recent_past_due_3160_date.ToString();
                if (reportData.Most_recent_past_due_6190_date != null)
                    Most_Recent_Past_Due_6190_Date = reportData.Most_recent_past_due_6190_date.ToString();
                if (reportData.Most_recent_past_due_91_plus_date != null)
                    Most_Recent_Past_Due_91_Plus_Date = reportData.Most_recent_past_due_91_plus_date.ToString();
                if (reportData.Most_recent_past_due_3160_never != null)
                    Most_Recent_Past_Due_3160_Never = reportData.Most_recent_past_due_3160_never;
                if (reportData.Most_recent_past_due_6190_never != null)
                    Most_Recent_Past_Due_6190_Never = reportData.Most_recent_past_due_6190_never;
                if (reportData.Most_recent_past_due_91_plus_never != null)
                    Most_Recent_Past_Due_91_Plus_Never = reportData.Most_recent_past_due_91_plus_never;
                if (reportData.Most_recent_loss_amt_date != null)
                    Most_Recent_Loss_Amt_Date = reportData.Most_recent_loss_amt_date.ToString();
                if (reportData.Most_recent_loss_status_date != null)
                    Most_Recent_Loss_Status_Date = reportData.Most_recent_loss_status_date.ToString();
                if (reportData.Recent_avg_days_past_due != null)
                    Recent_Avg_Days_Past_Due = reportData.Recent_avg_days_past_due.ToString();
                if (reportData.Recent_avg_days_past_due_unknown_ind != null)
                    Recent_Avg_Days_Past_Due_Unknown_Ind = reportData.Recent_avg_days_past_due_unknown_ind;
                if (reportData.Hist_avg_days_past_due != null)
                    Hist_Avg_Days_Past_Due = reportData.Hist_avg_days_past_due.ToString();
                if (reportData.Hist_avg_days_past_due_unknown_ind != null)
                    Hist_Avg_Days_Past_Due_Unknown_Ind = reportData.Hist_avg_days_past_due_unknown_ind;
                if (reportData.Contract_detail_included_cnt != null)
                    Contract_Detail_Included_Cnt = reportData.Contract_detail_included_cnt;
                if (reportData.Member_lenders != null)
                    MemberLenders = new MemberLenders(reportData.Member_lenders.Member_lender);
                if (reportData.Includes_others_contracts != null)
                    IncludesOthersContracts = reportData.Includes_others_contracts;
                if (reportData.Divergent_credit_quality != null)
                    DivergentCreditQuality = reportData.Divergent_credit_quality;
                if (reportData.Sic_Codes != null)
                    SicCodes = reportData.Sic_Codes.Sic_code.Select(p => new SicCode(p)).ToList<ISicCode>();
                if (reportData.Legal_names_max != null)
                    LegalNamesMax = reportData.Legal_names_max;
                if (reportData.Legal_names_note != null)
                    LegalNamesNote = reportData.Legal_names_note;
                if (reportData.Legal_names != null)
                    LegalNames = reportData.Legal_names.Legal_name.Select(p => new LegalName(p)).ToList<ILegalName>(); 
                if (reportData.Business_background != null)
                    BusinessBackground = new BusinessBackground(reportData.Business_background);
                if (reportData.Ucc_filings != null)
                    UccFilings = new UccFilings(reportData.Ucc_filings);

            }
        }

        public string GenerationDateTime { get; set; }

        public string PaynetId { get; set; }

        public string ReportId { get; set; }

        public string GeneratedForMember { get; set; }

        [JsonConverter(typeof(InterfaceConverter<INameAddress, NameAddress>))]
        public INameAddress NameAddress { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IInquiry, Inquiry>))]
        public List<IInquiry> inquiries { get; set; }

        public string MasterScore { get; set; }

        public string MasterScorePercentile { get; set; }

        public string MasterScoreKeyFactor_1 { get; set; }

        public string MasterScoreKeyFactor_2 { get; set; }

        public string MasterScoreKeyFactor_3 { get; set; }

        public string PrimaryEquipmentTypeCode { get; set; }

        public string PrimaryEquipmentTypeDesc { get; set; }

        public string OldestContractStartDate { get; set; }

        public string LastActivityReportedDate { get; set; }

        public string OpenContractsCnt { get; set; }

        public string ClosedContractsCnt { get; set; }

        public string OrigReceivableAmt { get; set; }

        public string HighCreditAmt { get; set; }

        public string AvgHighCreditPerLenderAmt { get; set; }

        public string AvgOrigTermMonths { get; set; }

        public string CurBalAmt { get; set; }

        public string CurBalAmtPctOfHighCredit { get; set; }

        public string CurBalAmtPctOfOrigReceivable { get; set; }

        public string MonthlyPaymentAmt { get; set; }

        public string annualized_scheduled_pmts { get; set; }

        public string LossAmt { get; set; }

        public string ScheduledPayments { get; set; }

        public string PastDue_3160_Occurrences { get; set; }

        public string PastDue_6190_Occurrences { get; set; }

        public string PastDue_91_Plus_Occurrences { get; set; }

        public string Past_Due_31_Plus_Occurrences { get; set; }

        public string Cur_Bal_Cur_30_Amt { get; set; }

        public string CurBalPastDue_3160_Amt { get; set; }

        public string CurBalPastDue_6190_Amt { get; set; }

        public string CurBalPastDue_91_Plus_Amt { get; set; }

        public string CurBalPastDue_31_Plus_Amt { get; set; }

        public string PastDue_3160_Amt { get; set; }

        public string PastDue_6190_Amt { get; set; }

        public string PastDue_91_Plus_Amt { get; set; }

        public string PastDue_31_Plus_Amt { get; set; }

        public string Cur_30_AmtPctOfCurBal { get; set; }

        public string PastDue_3160_Amt_Pct_Of_Cur_Bal { get; set; }

        public string Past_Due_6190_Amt_Pct_Of_Cur_Bal { get; set; }

        public string Past_Due_91_Plus_Amt_Pct_Of_Cur_Bal { get; set; }

        public string Past_Due_31_Plus_Amt_Pct_Of_Cur_Bal { get; set; }

        public string Most_Recent_Past_Due_3160_Date { get; set; }

        public string Most_Recent_Past_Due_6190_Date { get; set; }

        public string Most_Recent_Past_Due_91_Plus_Date { get; set; }

        public string Most_Recent_Past_Due_3160_Never { get; set; }

        public string Most_Recent_Past_Due_6190_Never { get; set; }

        public string Most_Recent_Past_Due_91_Plus_Never { get; set; }

        public string Most_Recent_Loss_Amt_Date { get; set; }

        public string Most_Recent_Loss_Status_Date { get; set; }

        public string Recent_Avg_Days_Past_Due { get; set; }

        public string Recent_Avg_Days_Past_Due_Unknown_Ind { get; set; }

        public string Hist_Avg_Days_Past_Due { get; set; }

        public string Hist_Avg_Days_Past_Due_Unknown_Ind { get; set; }

        public string Contract_Detail_Included_Cnt { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IMemberLenders, MemberLenders>))]
        public IMemberLenders MemberLenders { get; set; }

        public string IncludesOthersContracts { get; set; }

        public string DivergentCreditQuality { get; set; }




        [JsonConverter(typeof(InterfaceListConverter<ISicCode, SicCode>))]
        public List<ISicCode> SicCodes { get; set; }

        public string LegalNamesMax { get; set; }
        public string LegalNamesNote { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ILegalName, LegalName>))]
        public List<ILegalName> LegalNames { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IBusinessBackground, BusinessBackground>))]
        public IBusinessBackground BusinessBackground { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IUccFilings, UccFilings>))]
        public IUccFilings UccFilings { get; set; }

    }
}