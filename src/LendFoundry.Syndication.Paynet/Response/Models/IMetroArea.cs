﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IMetroArea
    {
        string Code { get; set; }
        string Text { get; set; }
    }
}
