﻿namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IContracts
    {
        INameAddress NameAddress { get; set; }

        string contract_type { get; set; }

        string equipment_type { get; set; }

        string guarantor_code { get; set; }

        string term { get; set; }

        string payment_amt { get; set; }

        string payment_freq { get; set; }

        string orig_receivable_amt { get; set; }

        string cur_bal_amt { get; set; }

        string cur_bal_pct_of_orig_receivable { get; set; }

        string past_due_3160_amt { get; set; }

        string past_due_6190_amt { get; set; }

        string past_due_91_plus_amt { get; set; }

        string scheduled_payments { get; set; }

        string past_due_3160_occurrences { get; set; }

        string past_due_91_plus_occurrences { get; set; }

        string days_past_due { get; set; }

        string avg_days_past_due { get; set; }

        string max_days_past_due_low { get; set; }

        string max_days_past_due_high { get; set; }

        string loss_amt { get; set; }

        string renewal_date { get; set; }

        string closed_date { get; set; }

        string as_of_date { get; set; }

        string start_date { get; set; }

        string last_payment_date { get; set; }

        string next_payment_due_date { get; set; }

        string max_past_due_date { get; set; }

        string loss_status { get; set; }

        string contract_open_ind { get; set; }

        string transferred_to_other_lender { get; set; }

        string transferred_from_other_lender { get; set; }

        string first_bal_date { get; set; }
    }
}