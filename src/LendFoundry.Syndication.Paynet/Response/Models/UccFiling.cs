﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public class UccFiling : IUccFiling
    {
        public UccFiling(Proxy.ReportResponse.Ucc_filing ucFilling)
        {
            if (null != ucFilling)
            {
                FilingDate = ucFilling.Filing_date;
                LegalAction = ucFilling.Legal_action;
                DocumentNumber = ucFilling.Document_number;
                FilingLocation = ucFilling.Filing_location;
                SecuredParty = ucFilling.Secured_party;
                Name = ucFilling.Name;
                Address1 = ucFilling.Address_1;
                City = ucFilling.City;
                StateCode = ucFilling.State_code;

            }
        }

        public string FilingDate { get; set; }

        public string LegalAction { get; set; }

        public string DocumentNumber { get; set; }

        public string FilingLocation { get; set; }

        public string SecuredParty { get; set; }

        public ICollateralItems CollateralItems { get; set; }

        public string Name { get; set; }

        public string Address1 { get; set; }

        public string City { get; set; }

        public string StateCode { get; set; }
    }
}
