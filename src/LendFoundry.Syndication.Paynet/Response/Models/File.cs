﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class File : IFile
    {
        public File(LendFoundry.Syndication.Proxy.PayNetDirect.File files)
        {
            if (files != null)
            {
                bytesField = files.Bytes;
                contentTypeField = files.ContentType;
                fileNameField = files.FileName;
                fileSizeField = files.FileSize;
            }
        }

        public byte[] bytesField { get; set; }

        public string contentTypeField { get; set; }

        public string fileNameField { get; set; }

        public int fileSizeField { get; set; }
    }
}