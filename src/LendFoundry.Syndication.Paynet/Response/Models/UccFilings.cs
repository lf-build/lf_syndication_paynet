﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public class UccFilings:IUccFilings
    {
        public UccFilings(Proxy.ReportResponse.Ucc_filings uccFilings)
        {
            if (null != uccFilings.Ucc_summary)
                UccSummary = new UccSummary(uccFilings.Ucc_summary);
            if (null != uccFilings.Ucc_detail)
                UccDetail = new UccDetail(uccFilings.Ucc_detail);

        }
        public IUccSummary UccSummary { get; set; }
        public IUccDetail UccDetail { get; set; }
    }
}
