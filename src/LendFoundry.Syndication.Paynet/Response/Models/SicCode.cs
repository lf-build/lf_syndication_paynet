﻿
namespace LendFoundry.Syndication.Paynet.Response
{
    public class SicCode : ISicCode
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sicCode"></param>
        public SicCode(Proxy.ReportResponse.Sic_Code sicCode)
        {
            if (sicCode != null)
            {
                Code = sicCode.Code.ReplaceIfNotNull();
                Text = sicCode.Text.ReplaceIfNotNull();
            }
        }

        public string Code { get; set; }
        public string Text { get; set; }
    }
}
