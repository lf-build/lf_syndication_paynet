﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IReportOption
    {
        string report_option { get; set; }
    }
}