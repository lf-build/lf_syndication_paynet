﻿
namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IManager
    {
        string Title { get; set; }
        string Text { get; set; }
    }
}
