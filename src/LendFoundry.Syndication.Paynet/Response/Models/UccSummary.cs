﻿

using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class UccSummary : IUccSummary
    {
        public UccSummary(Proxy.ReportResponse.Ucc_summary uccSummary)
        {
            if (null != uccSummary)
            {
                if (null != uccSummary.Ucc_summary_counts)
                    UccSummaryCounts = uccSummary.Ucc_summary_counts.Ucc_summary_count.Select(p => new UccSummaryCount(p)).ToList<IUccSummaryCount>();
                if (uccSummary.Ucc_summary_totals != null)
                    UccSummaryTotals = new UccSummaryTotals(uccSummary.Ucc_summary_totals);

                Xsi = uccSummary.Xsi;
            }
        }
        public List<IUccSummaryCount> UccSummaryCounts { get; set; }
        public IUccSummaryTotals UccSummaryTotals { get; set; }
        public string Xsi { get; set; }
    }
}
