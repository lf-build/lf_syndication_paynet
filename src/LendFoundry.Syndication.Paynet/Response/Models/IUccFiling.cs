﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IUccFiling
    {
        string FilingDate { get; set; }

        string LegalAction { get; set; }

        string DocumentNumber { get; set; }

        string FilingLocation { get; set; }

        string SecuredParty { get; set; }

        ICollateralItems CollateralItems { get; set; }

        string Name { get; set; }

        string Address1 { get; set; }

        string City { get; set; }

        string StateCode { get; set; }
    }
}
