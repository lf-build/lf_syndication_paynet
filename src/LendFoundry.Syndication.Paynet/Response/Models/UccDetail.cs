﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public class UccDetail :IUccDetail
    {
        public UccDetail(Proxy.ReportResponse.Ucc_detail uccDetail)
        {
            if (null != uccDetail)
            {
                if (uccDetail.Ucc_filing != null)
                    UccFiling = new UccFiling(uccDetail.Ucc_filing);

                Xsi = uccDetail.Xsi;
            }

        }
        public IUccFiling UccFiling { get; set; }
        public string Xsi { get; set; }
    }
}
