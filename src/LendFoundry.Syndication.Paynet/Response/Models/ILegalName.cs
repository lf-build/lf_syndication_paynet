﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface ILegalName
    {
        string LegalCorpName { get; set; }
        string LegalAddress1 { get; set; }
        string LegalAddress2 { get; set; }
        string LegalCity { get; set; }
        string LegalStateCode { get; set; }
        string LegalPostalCode { get; set; }
        string LegalEntityType { get; set; }
        string LegalFilingState { get; set; }
        string LegalStatus { get; set; }
        string LegalDateIncorporated { get; set; }
        string LegalLastYearlyReportDate { get; set; }
        string LegalFilingNum { get; set; }
        string LegalOfficerName1 { get; set; }
        string LegalOfficerTitle1 { get; set; }
        string LegalOfficerName2 { get; set; }
        string LegalOfficerTitle2 { get; set; }
        string LegalOfficerName3 { get; set; }
        string LegalOfficerTitle3 { get; set; }
        string LegalOfficerName4 { get; set; }
        string LegalOfficerTitle4 { get; set; }
        string LegalOfficerName5 { get; set; }
        string LegalOfficerTitle5 { get; set; }
        string LegalTaxid { get; set; }
    }
}
