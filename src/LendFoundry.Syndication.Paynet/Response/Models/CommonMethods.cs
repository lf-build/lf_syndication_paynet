﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response
{
    public static class CommonMethods
    {
        public static string ReplaceIfNotNull(this string Value)
        {
            if (Value!=null)
                return Value.Trim();
            return Value;
        }
    }
}
