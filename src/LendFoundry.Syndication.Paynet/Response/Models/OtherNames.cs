﻿

using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Syndication.Paynet.Response
{
    public class OtherNames: IOtherNames
    {
        public OtherNames(Paynet.Proxy.ReportResponse.other_names otherName)
        {
            if (null != otherName)
            {
                Name = otherName.name.Select(s => s.ReplaceIfNotNull()).ToList();
            }

        }
        public List<string> Name { get; set; }
    }
}
