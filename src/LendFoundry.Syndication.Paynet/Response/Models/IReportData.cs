﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IReportData
    {
        string GenerationDateTime { get; set; }

        string PaynetId { get; set; }

        string ReportId { get; set; }

        string GeneratedForMember { get; set; }

        INameAddress NameAddress { get; set; }

        List<IInquiry> inquiries { get; set; }

        string MasterScore { get; set; }

        string MasterScorePercentile { get; set; }

        string MasterScoreKeyFactor_1 { get; set; }

        string MasterScoreKeyFactor_2 { get; set; }

        string MasterScoreKeyFactor_3 { get; set; }

        string PrimaryEquipmentTypeCode { get; set; }

        string PrimaryEquipmentTypeDesc { get; set; }

        string OldestContractStartDate { get; set; }

        string LastActivityReportedDate { get; set; }

        string OpenContractsCnt { get; set; }

        string ClosedContractsCnt { get; set; }

        string OrigReceivableAmt { get; set; }

        string HighCreditAmt { get; set; }

        string AvgHighCreditPerLenderAmt { get; set; }

        string AvgOrigTermMonths { get; set; }

        string CurBalAmt { get; set; }

        string CurBalAmtPctOfHighCredit { get; set; }

        string CurBalAmtPctOfOrigReceivable { get; set; }

        string MonthlyPaymentAmt { get; set; }

        string annualized_scheduled_pmts { get; set; }

        string LossAmt { get; set; }

        string ScheduledPayments { get; set; }

        string PastDue_3160_Occurrences { get; set; }

        string PastDue_6190_Occurrences { get; set; }

        string PastDue_91_Plus_Occurrences { get; set; }

        string Past_Due_31_Plus_Occurrences { get; set; }

        string Cur_Bal_Cur_30_Amt { get; set; }

        string CurBalPastDue_3160_Amt { get; set; }

        string CurBalPastDue_6190_Amt { get; set; }

        string CurBalPastDue_91_Plus_Amt { get; set; }

        string CurBalPastDue_31_Plus_Amt { get; set; }

        string PastDue_3160_Amt { get; set; }

        string PastDue_6190_Amt { get; set; }

        string PastDue_91_Plus_Amt { get; set; }

        string PastDue_31_Plus_Amt { get; set; }

        string Cur_30_AmtPctOfCurBal { get; set; }

        string PastDue_3160_Amt_Pct_Of_Cur_Bal { get; set; }

        string Past_Due_6190_Amt_Pct_Of_Cur_Bal { get; set; }

        string Past_Due_91_Plus_Amt_Pct_Of_Cur_Bal { get; set; }

        string Past_Due_31_Plus_Amt_Pct_Of_Cur_Bal { get; set; }

        string Most_Recent_Past_Due_3160_Date { get; set; }

        string Most_Recent_Past_Due_6190_Date { get; set; }

        string Most_Recent_Past_Due_91_Plus_Date { get; set; }

        string Most_Recent_Past_Due_3160_Never { get; set; }

        string Most_Recent_Past_Due_6190_Never { get; set; }

        string Most_Recent_Past_Due_91_Plus_Never { get; set; }

        string Most_Recent_Loss_Amt_Date { get; set; }

        string Most_Recent_Loss_Status_Date { get; set; }

        string Recent_Avg_Days_Past_Due { get; set; }

        string Recent_Avg_Days_Past_Due_Unknown_Ind { get; set; }

        string Hist_Avg_Days_Past_Due { get; set; }

        string Hist_Avg_Days_Past_Due_Unknown_Ind { get; set; }

        string Contract_Detail_Included_Cnt { get; set; }

        IMemberLenders MemberLenders { get; set; }

        string IncludesOthersContracts { get; set; }

        string DivergentCreditQuality { get; set; }

        List<ISicCode> SicCodes { get; set; }

        string LegalNamesMax { get; set; }
        string LegalNamesNote { get; set; }

        List<ILegalName> LegalNames { get; set; }

        IBusinessBackground BusinessBackground { get; set; }


        IUccFilings UccFilings { get; set; }
    }
}