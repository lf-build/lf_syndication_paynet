﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet.Response {
    public interface ICompany {
        string PayNetId { get; set; }
        string Name { get; set; }

        List<string> Aliasses { get; set; }

        string TaxId { get; set; }

        string Street { get; set; }
        string City { get; set; }

        string StateCode { get; set; }
        string PostalCode { get; set; }
        string CountryCode { get; set; }

        List<IAddresses> AdditionalAddresses { get; set; }
        string Telephone { get; set; }
        List<string> AdditionalPhones { get; set; }
        string MatchScore { get; set; }
    }
}