﻿
namespace LendFoundry.Syndication.Paynet.Response
{
    public class MetroArea :IMetroArea
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="metroArea"></param>
        public MetroArea(Proxy.ReportResponse.Metro_area metroArea)
        {
            if (null != metroArea)
            {
                Code = metroArea.Code;
                Text = metroArea.Text;
            }

        }

        public string Code { get; set; }
        public string Text { get; set; }
    }
}
