﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public class UccSummaryTotals : IUccSummaryTotals
    {
        public UccSummaryTotals(Proxy.ReportResponse.Ucc_summary_totals uccSummaryTotals)
        {
            if (null != uccSummaryTotals)
            {
                TotalFiled = uccSummaryTotals.Total_filed;
                Cautionary = uccSummaryTotals.Cautionary;
                ReleasedAndTerminated = uccSummaryTotals.Released_and_terminated;
                Continued = uccSummaryTotals.Continued;
                AmendedAndAssigned = uccSummaryTotals.Amended_and_assigned;
            }


        }
        public string TotalFiled { get; set; }

        public string Cautionary { get; set; }

        public string ReleasedAndTerminated { get; set; }

        public string Continued { get; set; }

        public string AmendedAndAssigned { get; set; }
    }
}
