﻿

namespace LendFoundry.Syndication.Paynet.Response
{
    public interface IUccSummaryCount
    {
         string DateRange { get; set; }
         string TotalFiled { get; set; }
         string Cautionary { get; set; }
         string ReleasedAndTerminated { get; set; }
         string Continued { get; set; }
         string AmendedAndAssigned { get; set; }
    }
}
