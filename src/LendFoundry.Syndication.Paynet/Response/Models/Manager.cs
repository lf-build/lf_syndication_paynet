﻿
namespace LendFoundry.Syndication.Paynet.Response
{
    public class Manager :IManager
    {
        public Manager(Paynet.Proxy.ReportResponse.Manager manager)
        {
            if (null != manager)
            {
                Title = manager.Title;
                Text = manager.Text;
            }

        }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
