﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.Paynet.Response {
    public class GetReportResponse : IGetReportResponse {
        public object xmlField { get; set; }

        public string htmlField { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<IFile, File>))]
        public List<IFile> filesField { get; set; }

        public int errorCodeField { get; set; }

        public string errorDescriptionField { get; set; }

        public GetReportResponse () { }

        public GetReportResponse (LendFoundry.Syndication.Proxy.PayNetDirect.GetReportResponse response) {
            if (response != null) {
                // if (!string.IsNullOrEmpty (InnerXml (response))) {
                //     xmlField = XmlResponse (OuterXml (response.Xml));
                // }
                if (response != null)
                    htmlField = response.Body.GetReportResult.Html;
                if (response.Body.GetReportResult.Files != null)
                    filesField = response.Body.GetReportResult.Files.Select (file => new File (file))
                    .ToList<IFile> ();
                if (response.Body.GetReportResult.ErrorCode != 0)
                    errorCodeField = response.Body.GetReportResult.ErrorCode;
                if (response.Body.GetReportResult.ErrorDescription != null)
                    errorDescriptionField = response.Body.GetReportResult.ErrorDescription;
            }
        }

        public object XmlResponse (string xml) {
            var response = XmlConvertor.Deserialize<Proxy.ReportResponse.GetReportResponse> (xml);
            return new GetReportResult (response);
        }

        public static string OuterXml (XElement thiz) {
            var xReader = thiz.CreateReader ();
            xReader.MoveToContent ();
            return xReader.ReadOuterXml ();
        }

        public static string InnerXml (XElement thiz) {
            var xReader = thiz.CreateReader ();
            xReader.MoveToContent ();
            return xReader.ReadInnerXml ();
        }
    }
}