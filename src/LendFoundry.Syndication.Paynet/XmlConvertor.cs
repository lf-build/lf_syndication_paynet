﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
using System.Runtime.Serialization;

namespace LendFoundry.Syndication.Paynet
{
    [Serializable]
    public static class XmlConvertor
    {
        public static string Serialize<T>(T obj, Encoding encoding)
        {
            encoding = null;
            if (obj != null)
            {
                Type objType = obj.GetType();
                XmlSerializer serializer = new XmlSerializer(objType);
                string xml = string.Empty;
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;

                using (var stringWriter = new ExtentedStringWriter(encoding))
                {
                    using (XmlWriter writer = XmlWriter.Create(stringWriter))
                    {
                        XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                        namespaces.Add("s", "http://schemas.xmlsoap.org/soap/envelope/");
                        serializer.Serialize(writer, obj, namespaces);
                        xml = stringWriter.ToString();
                    }
                }
                return xml;
            }
            return string.Empty;
        }

        public static T Deserialize<T>(string xmlString)
        {
            Type objType = typeof(T);
            XmlSerializer serializer = new XmlSerializer(objType);
            var objectResult = (T)Activator.CreateInstance(objType);

            using (var stringReader = new StringReader(xmlString))
            {
                using (XmlReader reader = XmlReader.Create(stringReader))
                {
                    objectResult = (T)serializer.Deserialize(reader);
                }
            }
            return objectResult;
        }

        private static string ExtractSoapBody(string soapXml)
        {
            string extractedXml = string.Empty;
            XDocument docx = XDocument.Load(new StringReader(soapXml));
            var unwrappedResponse = docx.Descendants((XNamespace)"http://schemas.xmlsoap.org/soap/envelope/" + "Envelope")
                .FirstOrDefault()
                .FirstNode;
            docx.Descendants().Attributes("error_code").Remove();
            docx.Descendants().Attributes("version").Remove();

            var rdr = unwrappedResponse.CreateReader();
            rdr.MoveToContent();
            extractedXml = rdr.ReadInnerXml();
            return extractedXml;
        }
    }
}