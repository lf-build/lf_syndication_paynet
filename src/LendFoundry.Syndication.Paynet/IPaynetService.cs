﻿using LendFoundry.Syndication.Paynet.Request;
using LendFoundry.Syndication.Paynet.Response;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.Paynet
{
    public interface IPaynetService
    {
        Task<LendFoundry.Syndication.Paynet.Response.ISearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchCompanyRequest searchRequest);

        Task<IGetReportResponse> GetCompanyReport(string entityType, string entityId, IGetReportRequest reportRequest);
    }
}