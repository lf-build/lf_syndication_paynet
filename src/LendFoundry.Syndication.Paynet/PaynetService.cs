﻿using System;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Syndication.Paynet.Events;
using LendFoundry.Syndication.Paynet.Proxy;
using LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce;
using LendFoundry.Syndication.Paynet.Response;

namespace LendFoundry.Syndication.Paynet {
    public class PaynetService : IPaynetService {
        #region Private Properties

        private IPaynetConfiguration ReportConfiguration { get; }
        private IEventHubClient EventHub { get; }
        private ILookupService Lookup { get; }
        private IPaynetProxy Proxy { get; }
        private ITenantTime TenantTime { get; }
        private ILogger Logger { get; }

        #endregion Private Properties

        public PaynetService (IPaynetProxy proxy, IPaynetConfiguration reportConfiguration, IEventHubClient eventHub, ILookupService lookup, ILogger logger, ITenantTime tenantTime) {
            if (reportConfiguration == null)
                throw new ArgumentNullException (nameof (reportConfiguration));
            if (eventHub == null)
                throw new ArgumentNullException (nameof (eventHub));
            if (lookup == null)
                throw new ArgumentNullException (nameof (lookup));
            ReportConfiguration = reportConfiguration;
            EventHub = eventHub;
            Lookup = lookup;
            Proxy = proxy;
            Logger = logger;
            TenantTime = tenantTime;
        }

        public async Task<Response.ISearchCompanyResponse> SearchCompany (string entityType, string entityId, Request.ISearchCompanyRequest searchRequest) {
            #region Validation
            entityType = EnsureEntityType (entityType);

            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (searchRequest == null)
                throw new ArgumentNullException (nameof (searchRequest));

            if (string.IsNullOrWhiteSpace (searchRequest.CompanyName))
                throw new ArgumentNullException (nameof (searchRequest.CompanyName));

            if (searchRequest.CompanyName.Length > ReportConfiguration.CompanyNameLength)
                throw new ArgumentException ($"Company Name can not be more then { ReportConfiguration.CompanyNameLength}");

            if (string.IsNullOrWhiteSpace (searchRequest.StateCode))
                throw new ArgumentException ("State Code is Required", nameof (searchRequest.StateCode));

            if (searchRequest.StateCode.Length != ReportConfiguration.StateCodeLength)
                throw new ArgumentException ($"StateCode can not be more then { ReportConfiguration.StateCodeLength}");
            #endregion Validation

            try {
                Logger.Info ("Started Execution for SearchCompany Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf PayNet: Company Search");

                var proxyRequest = new Proxy.SearchCompanyRequest (searchRequest, ReportConfiguration);
                LendFoundry.Syndication.Proxy.PayNetDirect.CompanySearchResponse response = Proxy.SearchCompany (proxyRequest);
                var result = new Response.SearchCompanyResponse (response);
                await EventHub.Publish (new PaynetSearchCompanyRequested {
                    EntityId = entityId,
                        EntityType = entityType,
                        Response = result,
                        Request = searchRequest,
                        ReferenceNumber = Guid.NewGuid ().ToString ("N")
                });
                Logger.Info ("Completed Execution for SearchCompany Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf PayNet: Company Search");

                return result;
            } catch (Exception exception) {
                Logger.Error ("Error While Processing SearchCompany Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf PayNet: Company Search");

                await EventHub.Publish (new PaynetSearchCompanyRequestFail {
                    EntityId = entityId,
                        EntityType = entityType,
                        Response = exception.Message,
                        Request = searchRequest,
                        ReferenceNumber = null
                });
                throw;
            }
        }

        public async Task<IGetReportResponse> GetCompanyReport (string entityType, string entityId, Request.IGetReportRequest reportRequest) {
            #region Validation
            entityType = EnsureEntityType (entityType);

            if (string.IsNullOrWhiteSpace (entityId))
                throw new ArgumentNullException (nameof (entityId));

            if (reportRequest == null)
                throw new ArgumentNullException (nameof (reportRequest));

            if (string.IsNullOrWhiteSpace (reportRequest.PaynetId))
                throw new ArgumentNullException (nameof (reportRequest.PaynetId));
            #endregion Validation

            try {
                Logger.Info ("Started Execution for GetCompanyReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf PayNet: Company Report");

                var referencenumber = Guid.NewGuid ().ToString ("N");
                var proxyRequest = new Proxy.GetReportRequest (reportRequest, ReportConfiguration);
                var response = Proxy.GetCompanyReport (proxyRequest);
                var result = new Response.GetReportResponse (response);
                await EventHub.Publish (new PaynetGetCompanyReportRequested {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = result,
                    Request = reportRequest,
                    ReferenceNumber = referencenumber
                    });
                    Logger.Info ("Completed Execution for GetCompanyReport Request Info: Date & Time:" + TenantTime.Now + " EntityType:" + entityType + " EntityId:" + entityId + " TypeOf PayNet: Company Report");

                    return result;
                    } catch (Exception exception) {
                    Logger.Error ("Error While Processing GetCompanyReport Date & Time:" + TenantTime.Now + " EntityId:" + entityId + " EntityType:" + entityType + "Exception" + exception.Message + " TypeOf PayNet: Company Report");

                    await EventHub.Publish (new PaynetGetCompanyReportRequestFail {
                    EntityId = entityId,
                    EntityType = entityType,
                    Response = exception.Message,
                    Request = reportRequest,
                    ReferenceNumber = null
                    });
                    throw;
            }
        }

        private string EnsureEntityType (string entityType) {
            if (string.IsNullOrWhiteSpace (entityType))
                throw new ArgumentNullException (nameof (entityType));

            entityType = entityType.ToLower ();
            var validEntityTypes = Lookup.GetLookupEntry ("entityTypes", entityType);

            if (validEntityTypes == null || !validEntityTypes.Any ())
                throw new InvalidArgumentException ("Invalid Entity Type");

            return entityType;
        }

    }
}