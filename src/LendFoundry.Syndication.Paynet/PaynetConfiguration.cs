﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Syndication.Paynet {
    public class PaynetConfiguration : IPaynetConfiguration, IDependencyConfiguration {
        // ToDo : Check possible values once we have document
        public string Baseurl { get; set; }
        public string PaynetSearchUrl { get; set; }
        public string PaynetReportUrl { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string ProductOption { get; set; }
        public string ProductType { get; set; }

        public string ReportFormat { get; set; }

        public string Score { get; set; }

        public string UserField { get; set; }

        public string Version { get; set; }

        public bool UseProxy { get; set; }

        public string NewMatchThreshold { get; set; }

        public int CompanyNameLength { get; set; }
        public int StateCodeLength { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}