﻿using System;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Syndication.Paynet;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Paynet.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class PaynetServiceClientFactory : IPaynetServiceClientFactory
    {
        #region Public Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="endpoint"></param>
        /// <param name="port"></param>

        public PaynetServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="uri"></param>
        public PaynetServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        #endregion Public Constructors

        #region Private Properties

        private IServiceProvider Provider { get; }
        private Uri Uri { get; }
        #endregion Private Properties

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenReader"></param>
        /// <returns></returns>
        public IPaynetService Create(ITokenReader tokenReader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(tokenReader, logger).Get("paynet");
            }
            var client = Provider.GetServiceClient(tokenReader, uri);
            return new PaynetService(client);
        }

        #endregion Public Methods
    }
}