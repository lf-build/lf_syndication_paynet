﻿using LendFoundry.Security.Tokens;
using LendFoundry.Syndication.Paynet;

namespace LendFoundry.Paynet.Client
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPaynetServiceClientFactory
    {
        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        IPaynetService Create(ITokenReader reader);

        #endregion Public Methods
    }
}