﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Client;
using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Request;
using LendFoundry.Syndication.Paynet.Response;
using RestSharp;

namespace LendFoundry.Paynet.Client
{
    /// <summary>
    /// class used for injecting IPaynet service.
    /// </summary>
    public class PaynetService : IPaynetService
    {
        #region Public Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        public PaynetService(IServiceClient client)
        {
            Client = client;
        }
        #endregion Public Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="searchRequest"></param>
        /// <returns></returns>
        public async Task<ISearchCompanyResponse> SearchCompany(string entityType, string entityId, ISearchCompanyRequest searchRequest)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/paynet/company/search", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(searchRequest);
            return await Client.ExecuteAsync<SearchCompanyResponse>(request);
        }

        /// <summary>
        /// GetCompanyReport
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="reportRequest"></param>
        /// <returns></returns>
        public async Task<IGetReportResponse> GetCompanyReport(string entityType, string entityId, IGetReportRequest reportRequest)
        {
            var request = new RestRequest("/{entitytype}/{entityid}/paynet/company/report", Method.POST);
            request.AddUrlSegment("entitytype", entityType);
            request.AddUrlSegment("entityid", entityId);
            request.AddJsonBody(reportRequest);
            return await Client.ExecuteAsync<GetReportResponse>(request);
        }

        #endregion Public Methods
    }
}
