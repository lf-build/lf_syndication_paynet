﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Paynet.Client
{
    /// <summary>
    /// all Service injection
    /// </summary>
    public static class PaynetServiceClientExtensions
    {
        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="services">services</param>
        /// <param name="endpoint">endpoint of service</param>
        /// <param name="port">port of service</param>
        /// <returns>Service</returns>
        public static IServiceCollection AddPaynetService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPaynetServiceClientFactory>(p => new PaynetServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPaynetServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        #endregion Public Methods
    }
}