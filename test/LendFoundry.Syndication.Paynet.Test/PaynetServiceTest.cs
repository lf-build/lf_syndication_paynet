﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Request;
using LendFoundry.Syndication.Paynet.Response;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace CreditExchange.Syndication.Paynet.Test
{
    public class PaynetServiceTest
    {
        public PaynetServiceTest()
        {
            EventHub = new Mock<IEventHubClient>();
            Lookup = new Mock<ILookupService>();
            Proxy = new Mock<LendFoundry.Syndication.Paynet.Proxy.IPaynetProxy>();
            reportConfiguration = new PaynetConfiguration
            {
                UseProxy = false,
                UserName = "Paynet_direct_test@capitalalliance.com",
                Version = "0310",
                ProductType = "CHR",
                Baseurl = "https://secure.paynetonline.com/direct/PayNetDirect.asmx",
                Score = "MAST",
                ProductOption = "PUBF",
                Password = "Grape!123",
                ReportFormat = "BNDL",
                UserField = "Account # 123456"
            };
            paynetservice = new PaynetService(Proxy.Object,reportConfiguration, EventHub.Object, Lookup.Object);
        }

        private Mock<IEventHubClient> EventHub { get; }
        private Mock<ILookupService> Lookup { get; }
        private Mock<LendFoundry.Syndication.Paynet.Proxy.IPaynetProxy> Proxy { get; }
        private PaynetService paynetservice { get; }
        private PaynetConfiguration reportConfiguration { get; }

        [Fact]
        public void SearchCompany_ArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.SearchCompany(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ISearchCompanyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.SearchCompany("application", It.IsAny<string>(), It.IsAny<ISearchCompanyRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.SearchCompany("application", "12345", It.IsAny<ISearchCompanyRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentException>(() => paynetservice.SearchCompany(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ISearchCompanyRequest>()));
            Proxy.Setup(x => x.SearchCompany(null)).Returns(It.IsAny<LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce.CompanySearchResponse>());
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.SearchCompany(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<ISearchCompanyRequest>()));

            SearchCompanyRequest CompanyNameNullRequest = new SearchCompanyRequest
            {
                CompanyName = "Veto's Tattoo Parlor",
                City = "Nokomis",
                StateCode = "TN",
                Phone = "5035551212",
                TaxId = "",
                UserField = "",
                NewMatchThreshold = "60",
                Alias = "",
                Address = ""
            };
            Proxy.Setup(x => x.SearchCompany(null)).Returns(It.IsAny<LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce.CompanySearchResponse>());
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.SearchCompany(It.IsAny<string>(), It.IsAny<string>(), CompanyNameNullRequest));
            SearchCompanyRequest StateCodeNullRequest = new SearchCompanyRequest
            {
                CompanyName = "Veto's Tattoo Parlor",
                City = "Nokomis",
                StateCode = "TN",
                Phone = "5035551212",
                TaxId = "",
                UserField = "",
                NewMatchThreshold = "60",
                Alias = "",
                Address = ""
            };
            Proxy.Setup(x => x.SearchCompany(null)).Returns(It.IsAny<LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce.CompanySearchResponse>());
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.SearchCompany(It.IsAny<string>(), It.IsAny<string>(), StateCodeNullRequest));
        }

        [Fact]
        public void GetCompanyReport_ArgumentNullException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.GetCompanyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetReportRequest>()));
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.GetCompanyReport("application", It.IsAny<string>(), It.IsAny<IGetReportRequest>()));
            Dictionary<string, string> categoryLookup = new Dictionary<string, string>();
            categoryLookup.Add("1", "application");
            Lookup.Setup(x => x.GetLookupEntry(It.IsAny<string>(), It.IsAny<string>())).Returns(categoryLookup);
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.GetCompanyReport("application", "12345", It.IsAny<IGetReportRequest>()));
            Assert.ThrowsAsync<ArgumentException>(() => paynetservice.GetCompanyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetReportRequest>()));
            Proxy.Setup(x => x.GetCompanyReport(null)).Returns(It.IsAny<LendFoundry.Syndication.Paynet.Proxy.PayNetDirect.ReportResponse>());
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.GetCompanyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetReportRequest>()));

            Proxy.Setup(x => x.GetCompanyReport(null)).Returns(It.IsAny<LendFoundry.Syndication.Paynet.Proxy.PayNetDirect.ReportResponse>());
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.GetCompanyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetReportRequest>()));

            GetReportRequest PaynetIdNullRequest = new GetReportRequest
            {
                PaynetId = null,
                ReportFormat = "BNDL"
            };
            Proxy.Setup(x => x.GetCompanyReport(null)).Returns(It.IsAny<LendFoundry.Syndication.Paynet.Proxy.PayNetDirect.ReportResponse>());
            Assert.ThrowsAsync<ArgumentNullException>(() => paynetservice.GetCompanyReport(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<IGetReportRequest>()));
        }
    }
}