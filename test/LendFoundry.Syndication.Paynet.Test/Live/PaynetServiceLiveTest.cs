﻿using LendFoundry.Syndication.Paynet.Proxy;
using System.Xml;
using Xunit;

namespace LendFoundry.Syndication.Paynet.Test.Live
{
    public class PaynetServiceLiveTest
    {
        private PaynetProxy paynetProxy { get; }
        private IPaynetConfiguration paynetConfiguration { get; }

        public PaynetServiceLiveTest()
        {
            paynetConfiguration = new PaynetConfiguration
            {
              
                  Baseurl= "https://secure.paynetonline.com/direct/PayNetDirect.asmx",
                  UserName= "Paynet_direct_test@capitalalliance.com",
                  Password= "Grape!123",
                  ProductOption= "PUBF",
                  ProductType= "CHR",
                  ReportFormat= "BNDL",
                  Score= "MAST",
                  UserField= "Account # 123456",
                  Version= "0310",
                  NewMatchThreshold= "60",
                  UseProxy= true

            };
            paynetProxy = new PaynetProxy(paynetConfiguration);
        }

        [Fact]
        public void SearchCompany_Response()
        {
            var response = paynetProxy.SearchCompany(new Proxy.SearchCompanyRequest
            {
                CompanyName = "Veto's Tattoo Parlor",
                City = "Nokomis",
                StateCode = "TN",
                Phone = "5035551212",
                TaxId = "",
                UserField = "",
                NewMatchThreshold = "60",
                Alias = "",
                Address = ""
            });
            Assert.NotNull(response);
        }

        [Fact]
        public void GetCompanyReport_Response()
        {

            var response = paynetProxy.GetCompanyReport(new Proxy.GetReportRequest
            {
                PaynetId = "59206906",
                ReportFormat = "BNDL",
                ProductOption = "PUBF",
                ProductType = "CHR",
                Score = "MAST",
                UserField = "Account # 123456",
                Version = "0310",

            });
            Assert.NotNull(response);
        }
    }
}