﻿using LendFoundry.Syndication.Paynet;
using LendFoundry.Syndication.Paynet.Proxy;
using System;
using Xunit;

namespace CreditExchange.Syndication.Paynet.Test
{
    public class PaynetConfigurationTest
    {
        [Fact]
        public void ThrowArgumentExceptionWithNullConfiguration()
        {
            Assert.Throws<ArgumentNullException>(() => new PaynetProxy(null));
        }

        [Fact]
        public void ThrowArgumentExceptionWithMissingConfiguration()
        {
            var paynetconfiguration = new PaynetConfiguration
            {
                UseProxy = false,
                UserName = "Paynet_direct_test@capitalalliance.com",
                Version = "0310",
                ProductType = "CHR",
                Baseurl = "https://secure.paynetonline.com/direct/PayNetDirect.asmx",
                Score = "MAST",
                ProductOption = "PUBF",
                Password = "",
                ReportFormat = "BNDL",
                UserField = "Account # 123456"
            };

            Assert.Throws<ArgumentNullException>(() => new PaynetProxy(paynetconfiguration));
            var paynetconfiguration1 = new PaynetConfiguration
            {
                UseProxy = false,
                UserName = " ",
                Version = "0310",
                ProductType = "CHR",
                Baseurl = "https://secure.paynetonline.com/direct/PayNetDirect.asmx",
                Score = "MAST",
                ProductOption = "PUBF",
                Password = "Grape!123",
                ReportFormat = "XML",
                UserField = "Account # 123456"
            };

            Assert.Throws<ArgumentNullException>(() => new PaynetProxy(paynetconfiguration1));
            var paynetconfiguration2 = new PaynetConfiguration
            {
                UseProxy = false,
                UserName = "Paynet_direct_test@capitalalliance.com",
                Version = " ",
                ProductType = "CHR",
                Baseurl = "https://secure.paynetonline.com/direct/PayNetDirect.asmx",
                Score = "MAST",
                ProductOption = "PUBF",
                Password = "Grape!123",
                ReportFormat = "PDF",
                UserField = "Account # 123456"
            };

            Assert.Throws<ArgumentNullException>(() => new PaynetProxy(paynetconfiguration2));
            var paynetconfiguration3 = new PaynetConfiguration
            {
                UseProxy = false,
                UserName = "Paynet_direct_test@capitalalliance.com",
                Version = "0310",
                ProductType = "CHR",
                Baseurl = "https://secure.paynetonline.com/direct/PayNetDirect.asmx",
                Score = "MAST",
                ProductOption = "PUBF",
                Password = "Grape!123",
                ReportFormat = null,
                UserField = "Account # 123456"
            };

            Assert.Throws<ArgumentNullException>(() => new PaynetProxy(paynetconfiguration3));

            var paynetconfiguration4 = new PaynetConfiguration
            {
                UseProxy = false,
                UserName = "Paynet_direct_test@capitalalliance.com",
                Version = "0310",
                ProductType = null,
                Baseurl = "https://secure.paynetonline.com/direct/PayNetDirect.asmx",
                Score = "MAST",
                ProductOption = "PUBF",
                Password = "Grape!123",
                ReportFormat = "PDF",
                UserField = "Account # 123456"
            };

            Assert.Throws<ArgumentNullException>(() => new PaynetProxy(paynetconfiguration4));

        }
    }
}