﻿using LendFoundry.Foundation.Services;
using LendFoundry.Paynet.Client;
using LendFoundry.Syndication.Paynet.Request;
using LendFoundry.Syndication.Paynet.Proxy.CompanySearchResponce;
using LendFoundry.Syndication.Paynet.Response;
using Moq;
using RestSharp;
using Xunit;
using Paynet;

namespace LendFoundry.Paynet.Client.Test
{
    public class PaynetServiceClientTest
    {
        private Mock<IServiceClient> Client { get; set; }
        public PaynetService paynetServiceClient { get; }

        private IRestRequest restRequest { get; set; }

        public PaynetServiceClientTest()
        {
            Client = new Mock<IServiceClient>();
            paynetServiceClient = new PaynetService(Client.Object);
        }

        [Fact]
        public async void client_SearchCompany()
        {
            CompanySearchResponse companySearchResponce = new CompanySearchResponse();
            companySearchResponce.Company = null;

            Client.Setup(s => s.ExecuteAsync<SearchCompanyResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new SearchCompanyResponse(companySearchResponce));

            var response = await paynetServiceClient.SearchCompany("application", "test", It.IsAny<ISearchCompanyRequest>());
            Assert.Equal("/{entitytype}/{entityid}/paynet/company/search", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }
          [Fact]
        public async void client_GetCompanyReport()
        {
            ReportResponse reportResponse = new ReportResponse();
            reportResponse = null;

            Client.Setup(s => s.ExecuteAsync<GetReportResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => restRequest = r).ReturnsAsync(new GetReportResponse(reportResponse));

            var response = await paynetServiceClient.GetCompanyReport("application", "test", It.IsAny<IGetReportRequest>());
            Assert.Equal("/{entitytype}/{entityid}/paynet/company/report", restRequest.Resource);
            Assert.Equal(Method.POST, restRequest.Method);
        }
    }
}